This work is licensed under the MIT License. See LICENSE.txt for more details.
Information about licenses for required packages can be found in the NOTICE.md file.

Execute server_start.sh to run the server (requires pm2).

Set a cron job for content_updater.sh once a day.
