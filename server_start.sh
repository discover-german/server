#!/bin/bash
cd /home/learnrec/ContentCrawler
source venv/bin/activate
export PYTHONPATH="${PYTHONPATH}:."
gunicorn --certfile=../certs/server.crt --keyfile=../certs/server.key --bind=0.0.0.0:54500 app:app
