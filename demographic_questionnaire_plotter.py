import os
import matplotlib.pyplot as plt
from openpyxl import load_workbook


def plot_demographic_questionnaires():
    wb_path = ""
    wb = load_workbook(wb_path)
    if not os.path.isdir("qfigs"):
        os.makedirs("qfigs")
    medienkonsum_news_articles = []
    medienkonsum_blogs = []
    medienkonsum_newspapers = []
    medienkonsum_ebooks = []
    medienkonsum_paperbooks = []
    medienkonsum_podcasts = []
    medienkonsum_videos = []
    medienkonsum_deutsch_news_articles = []
    medienkonsum_deutsch_blogs = []
    medienkonsum_deutsch_newspapers = []
    medienkonsum_deutsch_ebooks = []
    medienkonsum_deutsch_paperbooks = []
    medienkonsum_deutsch_podcasts = []
    medienkonsum_deutsch_videos = []
    for sheet, sheet_name in zip(wb.worksheets, wb.sheetnames):
        for col in sheet.columns:
            if col[1].value is None:
                break
            values = [x.value for x in col[2:22]]
            if None in values:
                values = list(filter(None, values))
            if "Alter" in col[1].value:
                ages = values.copy()
                mean = find_mean(ages)
                maxim = max(ages)
                minim = min(ages)
                variance, sd = find_variance(ages, 'sample')
                print(f"mean: {mean}, max: {maxim}, min: {minim}, sd: {sd}")
            elif "Niveau" in col[1].value:
                B1_count = 0
                B2_count = 0
                C1_count = 0
                C2_count = 0
                for value in values:
                    if value == 1:
                        B1_count += 1
                    elif value == 2:
                        B2_count += 1
                    elif value == 3:
                        C1_count += 1
                    elif value == 4:
                        C2_count += 1
                print(f"B1: {B1_count}, B2: {B2_count}, C1: {C1_count}, C2: {C2_count}")
            elif "Technische Erfahrung" in col[1].value:
                low_count = 0
                medium_count = 0
                high_count = 0
                for value in values:
                    if value == 1:
                        low_count += 1
                    elif value == 2:
                        medium_count += 1
                    elif value == 3:
                        high_count += 1
                print(f"low: {low_count}, medium: {medium_count}, high: {high_count}")
            elif "Lernapps" in col[1].value and "Yes" not in col[1].value:
                no_count = 0
                yes_count = 0
                for value in values:
                    if value == 1:
                        no_count += 1
                    elif value == 2:
                        yes_count += 1
                print(f"no: {no_count}, yes: {yes_count}")
            elif "Medienkonsum: Online news articles" in col[1].value:
                medienkonsum_news_articles = values.copy()
            elif "Medienkonsum: Blogs" in col[1].value:
                medienkonsum_blogs = values.copy()
            elif "Medienkonsum: Newspapers" in col[1].value:
                medienkonsum_newspapers = values.copy()
            elif "Medienkonsum: E-Books" in col[1].value:
                medienkonsum_ebooks = values.copy()
            elif "Medienkonsum: Paper books" in col[1].value:
                medienkonsum_paperbooks = values.copy()
            elif "Medienkonsum: Podcasts" in col[1].value:
                medienkonsum_podcasts = values.copy()
            elif "Medienkonsum: Videos/Series/Movies" in col[1].value:
                medienkonsum_videos = values.copy()
            elif "Medienkonsum Deutsch: Online news articles" in col[1].value:
                medienkonsum_deutsch_news_articles = values.copy()
            elif "Medienkonsum Deutsch: Blogs" in col[1].value:
                medienkonsum_deutsch_blogs = values.copy()
            elif "Medienkonsum Deutsch: Newspapers" in col[1].value:
                medienkonsum_deutsch_newspapers = values.copy()
            elif "Medienkonsum Deutsch: E-Books" in col[1].value:
                medienkonsum_deutsch_ebooks = values.copy()
            elif "Medienkonsum Deutsch: Paper books" in col[1].value:
                medienkonsum_deutsch_paperbooks = values.copy()
            elif "Medienkonsum Deutsch: Podcasts" in col[1].value:
                medienkonsum_deutsch_podcasts = values.copy()
            elif "Medienkonsum Deutsch: Videos/Series/Movies" in col[1].value:
                medienkonsum_deutsch_videos = values.copy()
        medienkonsum = [medienkonsum_news_articles, medienkonsum_blogs, medienkonsum_newspapers,
                        medienkonsum_ebooks, medienkonsum_paperbooks, medienkonsum_podcasts,
                        medienkonsum_videos]
        medienkonsum_titles = ["Online news articles", "Blogs", "Newspapers", "E-Books", "Paper books", "Podcasts",
                               "Videos/Series/Movies"]
        f, ax = plt.subplots(int(len(medienkonsum) / 4) + 1, 4, figsize=[16, 12])
        for i, mk in enumerate(medienkonsum):
            row = int(i / 4)
            col = i % 4
            mk_values_with_offset = [x - 1 for x in mk]
            ax[row, col].boxplot(mk_values_with_offset, labels=[""])
            ax[row, col].set_yticks(range(0, 5))
            if row == 0 and col == 0 or row == 1 and col == 0:
                ax[row, col].set_yticklabels(
                    ["Never", "A few times per year", "A few times per month", "A few times per week",
                     "Every day"])
            else:
                ax[row, col].set_yticklabels([""]*5)
            # plt.xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
            ax[row, col].set_ylim([0, 4.1])
            # plt.axhline(2, color="r")
            ax[row, col].set_title(f"{medienkonsum_titles[i]}")
        ax[-1, -1].axis('off')
        f.suptitle("Media Consumption (any language)", fontsize=18)
        plt.savefig("qfigs/media_consumption_any.svg")

        medienkonsum_deutsch = [medienkonsum_deutsch_news_articles, medienkonsum_deutsch_blogs,
                                medienkonsum_deutsch_newspapers, medienkonsum_deutsch_ebooks,
                                medienkonsum_deutsch_paperbooks, medienkonsum_deutsch_podcasts,
                                medienkonsum_deutsch_videos]
        f, ax = plt.subplots(int(len(medienkonsum_deutsch) / 4) + 1, 4, figsize=[16, 12])
        for i, mk in enumerate(medienkonsum_deutsch):
            row = int(i / 4)
            col = i % 4
            mk_values_with_offset = [x - 1 for x in mk]
            ax[row, col].boxplot(mk_values_with_offset, labels=[""])
            ax[row, col].set_yticks(range(0, 5))
            if row == 0 and col == 0 or row == 1 and col == 0:
                ax[row, col].set_yticklabels(
                    ["Never", "A few times per year", "A few times per month", "A few times per week",
                     "Every day"])
            else:
                ax[row, col].set_yticklabels([""] * 5)
            # plt.xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
            ax[row, col].set_ylim([0, 4.1])
            # plt.axhline(2, color="r")
            ax[row, col].set_title(f"{medienkonsum_titles[i]}")
        ax[-1, -1].axis('off')
        f.suptitle("Media Consumption (in German)", fontsize=18)
        plt.savefig("qfigs/media_consumption_german.svg")
        plt.show()


def find_variance(list_of_numbers, var_type):
    # argument check
    valid_var_types = ['sample', 'population']
    if var_type not in valid_var_types:
        raise ValueError(
            f'Incorrect argument for variance type passed: {var_type}! Allowed values \'sample\' or \'population\'')
    # define correct sequence length
    seq_length = len(list_of_numbers) - 1 if var_type == 'sample' else len(list_of_numbers)
    # using the previously defined function
    mean = find_mean(list_of_numbers)
    # find differences and squared differences
    differences = [number - mean for number in list_of_numbers]
    squared_differences = [x ** 2 for x in differences]
    # calculate
    variance = sum(squared_differences) / seq_length
    std = variance ** 0.5
    return round(variance, 2), round(std, 2)


def find_mean(list_of_numbers):
    sum_n = sum(list_of_numbers)
    len_n = len(list_of_numbers)
    mean = sum_n / len_n
    return mean


if __name__ == "__main__":
    plot_demographic_questionnaires()
