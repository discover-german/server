import copy
import logging
import bson
import datetime

from database import Database

db = Database()


def create_user(participant_id: str, personalised: bool = True):
    return db.create_user(participant_id, personalised)


def set_personalised(participant_id: str, personalised: bool):
    user = db.get_user_by_participant_id(participant_id)
    new_user = copy.deepcopy(user)
    new_user["personalised"] = personalised
    db.replace_user(user, new_user)


def show_personalised(participant_id: str) -> bool:
    user = db.get_user_by_participant_id(participant_id)
    return user["personalised"]


def flip_personalised(participant_id: str):
    user = db.get_user_by_participant_id(participant_id)
    new_user = copy.deepcopy(user)
    if "personalised" in user:
        new_user["personalised"] = not user["personalised"]
        db.replace_user(user, new_user)


def register_interest(participant_id: str, content_type: str, content_id: str, episode_index=None):
    if type(episode_index) == str:
        try:
            episode_index = int(episode_index)
        except Exception as e:
            logging.error("statistics", e)
            episode_index = 0
    if db.user_exists(participant_id):
        db.register_interest(participant_id, content_type, content_id, episode_index)
    return {"status": "success"}


def clear_logs():
    users = db.get_all_users()
    for user in users:
        new_user = copy.deepcopy(user)
        new_user["logs"] = []
        db.replace_user(user, new_user)


def clear_log(participant_id: str):
    user = db.get_user_by_participant_id(participant_id)
    new_user = copy.deepcopy(user)
    new_user["logs"] = []
    db.replace_user(user, new_user)


def show_users_with_activity():
    users = db.get_all_users()
    for user in users:
        if len(user["logs"]) > 0:
            print(user["participantId"])


def show_users_bson_size():
    users = db.get_all_users()
    for user in users:
        print(f"{user['participantId']}: {len(bson.BSON.encode(user))}")


def add_personalised_switch_time(participant_id: str, minute, hour, day, month=5, year=2022):
    user = db.get_user_by_participant_id(participant_id)
    new_user = copy.deepcopy(user)
    switch_time = datetime.datetime.strptime(f"{year}-{month}-{day}T{hour}:{minute}:00+0200", "%Y-%m-%dT%H:%M:%S%z")
    new_user["switch_time"] = switch_time.timestamp() * 1000  # time in millis
    db.replace_user(user, new_user)


if __name__ == "__main__":
    add_personalised_switch_time("test", 12, 16, 21, 5)
