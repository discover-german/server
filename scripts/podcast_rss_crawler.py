import copy
import logging
import time

from database import Database
from string_processing import StringProcessing
import feedparser
from pymongo.errors import DocumentTooLarge
from time import sleep
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

db = Database()
sp = StringProcessing()

options = webdriver.ChromeOptions()
options.add_argument("--headless")
service = Service("../chromedriver")
# driver = webdriver.Chrome(service=service, options=options)

def parse_rss_feeds_to_db():
    with open("rss_links", "r") as f:
        for rss_link in f.readlines():
            rss_feed = feedparser.parse(rss_link)
            print(rss_link)
            try:
                podcast = parse_rss_feed(rss_feed, rss_link)
                result = None
                while result is None:
                    try:
                        print("\r doing my thing", end="")
                        #result = db.save_rss_feed(podcast)
                    except DocumentTooLarge as e:
                        podcast["entries"].pop(0)
                        logging.error(f"Document was too large: {rss_link}")
                sleep(0.1)
            except AttributeError as e:
                print(e)
                pass
    return


def parse_rss_feed(rss_feed, rss_link):
    if "href" in rss_feed.feed.image:
        image_link = rss_feed.feed.image.href
    elif "url" in rss_feed.feed.image:
        print("found url")
        image_link = rss_feed.feed.image.url
    else:
        image_link = ""
    podcast = {
        "title": rss_feed.feed.title,
        "link": rss_feed.feed.link,
        "rss": rss_link,
        "description": rss_feed.feed.description,
        "image_link": image_link,
        "entries": []
    }
    entries = extract_entries_from_feed(rss_feed.entries, image_link)
    podcast["entries"] = entries
    return podcast

def extract_entries_from_feed(rss_feed_entries: list, image_link: str) -> list:
    entries = []
    for entry in rss_feed_entries:
        for link_candidate in entry.links:
            if "audio" in link_candidate.type:
                link = link_candidate.href
                break
        else:
            continue
        if "description" not in entry and "summary" not in entry and "keywords" not in entry:
            continue
        if "description" in entry:
            description = entry.description
        else:
            description = None
        if "itunes_summary" in entry:
            summary = entry.itunes_summary
        elif "summary" in entry:
            summary = entry.summary
        elif "subtitles" in entry:
            summary = entry.subtitle
        else:
            summary = None
        if "itunes_image" in entry:
            entry_image_link = entry.itunes_immage
        elif "image" in entry and "href" in entry.image:
            entry_image_link = entry.image.href
        else:
            entry_image_link = image_link
        if "itunes_keywords" in entry:
            keywords = entry.itunes_keywords
        elif "keywords" in entry:
            keywords = entry.keywords
        else:
            keywords = None
        if "description" in entry:
            readability_index = sp.calculate_readability_index(entry.description)
            difficulty = sp.get_difficulty_from_readability_index(readability_index)
        elif "summary" in entry:
            readability_index = sp.calculate_readability_index(entry.summary)
            difficulty = sp.get_difficulty_from_readability_index(readability_index)
        else:
            print("unable to calculate difficulty")
            difficulty = ""
        entries.append({
            "title": entry.title,
            "link": link,
            "description": description,
            "published": entry.published,
            "summary": summary,
            "image_link": entry_image_link,
            "keywords": keywords,
            "difficulty": difficulty
        })
    entries.reverse()
    return entries

def add_rss_links():
    with open("rss_links", "r") as f:
        for rss_link in f.readlines():
            try:
                rss_feed = feedparser.parse(rss_link)
                print(rss_link)
                podcast = db.find_podcast_with_title_and_link(rss_feed.feed.title, rss_feed.feed.link)
                if podcast is not None:
                    new_podcast = copy.deepcopy(podcast)
                    new_podcast["rss"] = rss_link.strip()
                    db.replace_podcast(podcast, new_podcast)
                else:
                    print(f"{rss_feed.feed.title} and {rss_feed.feed.link} not found")
                time.sleep(0.5)
            except Exception as e:
                print(e)
                pass

if __name__ == "__main__":
    parse_rss_feeds_to_db()
