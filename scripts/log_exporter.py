from enum import Enum

import csv
import pytz
from datetime import datetime
from openpyxl import Workbook
from database import Database

db = Database()


class Activities(Enum):
    ARTICLE_SESSION_START = 0
    ARTICLE_SESSION_END = 1
    BOOK_SESSION_START = 2
    BOOK_SESSION_END = 3
    PODCAST_SESSION_START = 4
    PODCAST_SESSION_END = 5
    WEBVIEW_READER_CLICK = 6
    SIMPLE_READER_CLICK = 7
    PARAGRAPH_READER_CLICK = 8
    APP_ON_RESUME = 9
    APP_ON_PAUSE = 10
    APP_ON_START = 11
    APP_ON_STOP = 12
    FLASHCARD_START = 13
    FLASHCARD_END = 14
    PHRASES_START = 15
    PHRASES_END = 16
    FILTER_START = 17
    FILTER_END = 18
    STATS_START = 19
    STATS_END = 20


class Notifications(Enum):
    ARTICLE_NOTIFICATION_CLICK = 0
    ARTICLE_NOTIFICATION_DISMISS = 1
    BOOK_NOTIFICATION_CLICK = 2
    BOOK_NOTIFICATION_DISMISS = 3
    PODCAST_NOTIFICATION_CLICK = 4
    PODCAST_NOTIFICATION_DISMISS = 5
    ARTICLE_NOTIFICATION_SET = 6
    BOOK_NOTIFICATION_SET = 7
    PODCAST_NOTIFICATION_SET = 8
    FLASHCARD_NOTIFICATION_SET = 9
    SURPRISE_NOTIFICATION_SET = 10


def export_user_logs_as_xlsx():
    users = db.get_all_users()
    for user in users:
        wb = Workbook()
        wb.iso_dates = True
        tz = pytz.timezone('Europe/Berlin')
        generic_sheet = wb.create_sheet("generic", 0)
        generic_sheet.append(["id", "event", "time", "title"])
        activities_sheet = wb.create_sheet("activities", 1)
        activities_sheet.append(["id", "event", "time", "title"])
        notifications_sheet = wb.create_sheet("notifications", 2)
        notifications_sheet.append(["id", "event", "time", "title"])
        sessions_sheet = wb.create_sheet("sessions", 3)
        sessions_sheet.append(["event", "duration", "start", "title"])
        stats_sheet = wb.create_sheet("stats", 4)
        stats_sheet.append(["total duration", "article duration", "book duration",
                            "podcast duration", "app duration", "total ntfctn clicks",
                            "article ntfctn clicks", "book ntfctn clicks",
                            "podcast ntfctn clicks", "flashcard ntfctn clicks",
                            "total ntfctn dismissals",
                            "article ntfctn dismissals", "book ntfctn dismissals",
                            "podcast ntfctn dismissals", "flashcard ntfctn dismissals",
                            "total ntfctn sets",
                            "article ntfctn sets", "book ntfctn sets",
                            "podcast ntfctn sets", "flashcard ntfctn sets",
                            "cleaned viewer clicks", "translated viewer clicks"])
        if "logs" in user:
            sessions = {}
            total_duration = 0
            app_duration = 0
            article_duration = 0
            book_duration = 0
            podcast_duration = 0
            total_notification_clicks = 0
            article_notification_clicks = 0
            book_notification_clicks = 0
            podcast_notification_clicks = 0
            flashcard_notification_clicks = 0
            total_notification_dismissals = 0
            article_notification_dismissals = 0
            book_notification_dismissals = 0
            podcast_notification_dismissals = 0
            flashcard_notification_dismissals = 0
            total_notification_sets = 0
            article_notification_sets = 0
            book_notification_sets = 0
            podcast_notification_sets = 0
            flashcard_notification_sets = 0
            cleaned_viewer_clicks = 0
            translated_viewer_clicks = 0
            logs = user["logs"]
            sorted_logs = sorted(logs, key=lambda i: i['time'])
            sorted_logs = _remove_duplictes(sorted_logs)
            first_half_logs = []
            second_half_logs = []
            for log in sorted_logs:
                if "switch_time" in user:
                    if log["time"] < user["switch_time"]:
                        first_half_logs.append(log)
                    else:
                        second_half_logs.append(log)
                title = ""
                if "oid" in log:
                    oid = log["oid"]
                    if oid != "":
                        if "ARTICLE" in log["type"]:
                            article = db.get_item(oid, "article")
                            if article is not None:
                                title = article["title"]
                            else:
                                old_article = db.get_article_from_archive(oid)
                                if old_article is not None:
                                    title = old_article["title"]
                        elif "BOOK" in log["type"]:
                            book = db.get_item(oid, "book")
                            if book is not None:
                                title = book["title"]
                        elif "PODCAST" in log["type"]:
                            podcast = db.get_item(oid, "podcast")
                            if podcast is not None:
                                title = podcast["title"]
                row = [log["id"], log["type"], datetime.fromtimestamp(log["time"] / 1000, tz).isoformat(), title]
                generic_sheet.append(row)
                if log["type"] in Activities.__members__:
                    activities_sheet.append(row)
                    if "START" in log["type"] or "RESUME" in log["type"]:
                        session_type = log["type"].split("_")[0]
                        sessions[session_type] = log["time"]
                    if "END" in log["type"] or "PAUSE" in log["type"] or "STOP" in log["type"]:
                        session_type = log["type"].split("_")[0]
                        if session_type in sessions:
                            duration = ((log["time"] - sessions[session_type]) / 1000)
                            sessions_sheet.append([session_type, duration,
                                                   datetime.fromtimestamp(sessions[session_type] / 1000, tz)
                                                  .isoformat(), title])
                            total_duration += duration
                            if "ARTICLE" in session_type:
                                article_duration += duration
                            elif "BOOK" in session_type:
                                book_duration += duration
                            elif "PODCAST" in session_type:
                                podcast_duration += duration
                            elif "APP" in session_type:
                                app_duration += duration
                elif log["type"] in Notifications.__members__:
                    notifications_sheet.append(row)
                    if "CLICK" in log["type"]:
                        total_notification_clicks += 1
                        if "ARTICLE" in log["type"]:
                            article_notification_clicks += 1
                        elif "BOOK" in log["type"]:
                            book_notification_clicks += 1
                        elif "PODCAST" in log["type"]:
                            podcast_notification_clicks += 1
                        elif "FLASHCARD" in log["type"]:
                            flashcard_notification_clicks += 1
                    elif "DISMISS" in log["type"]:
                        total_notification_dismissals += 1
                        if "ARTICLE" in log["type"]:
                            article_notification_dismissals += 1
                        elif "BOOK" in log["type"]:
                            book_notification_dismissals += 1
                        elif "PODCAST" in log["type"]:
                            podcast_notification_dismissals += 1
                        elif "FLASHCARD" in log["type"]:
                            flashcard_notification_dismissals += 1
                    elif "SET" in log["type"]:
                        total_notification_sets += 1
                        if "ARTICLE" in log["type"]:
                            article_notification_sets += 1
                        elif "BOOK" in log["type"]:
                            book_notification_sets += 1
                        elif "PODCAST" in log["type"]:
                            podcast_notification_sets += 1
                        elif "FLASHCARD" in log["type"]:
                            flashcard_notification_sets += 1
                if "SIMPLE_READER_CLICK" in log["type"]:
                    cleaned_viewer_clicks += 1
                elif "PARAGRAPH_READER_CLICK" in log["type"]:
                    translated_viewer_clicks += 1
            stats_sheet.append([total_duration, article_duration, book_duration, podcast_duration, app_duration,
                                total_notification_clicks, article_notification_clicks,
                                book_notification_clicks, podcast_notification_clicks, flashcard_notification_clicks,
                                total_notification_dismissals, article_notification_dismissals,
                                book_notification_dismissals, podcast_notification_dismissals,
                                flashcard_notification_dismissals,
                                total_notification_sets, article_notification_sets,
                                book_notification_sets, podcast_notification_sets, flashcard_notification_sets,
                                cleaned_viewer_clicks, translated_viewer_clicks])
            stats_sheet.append([])
            first_half = "Personalised"
            second_half = "Random"
            if "personalised" in user:
                if user["personalised"]:
                    first_half = "Random"
                    second_half = "Personalised"
                stats_sheet.append([f"Duration {first_half}"])
                first_half_total_duration = 0
                fh_sessions = {}
                for fh_log in first_half_logs:
                    if fh_log["type"] in Activities.__members__:
                        if "START" in fh_log["type"] or "RESUME" in fh_log["type"]:
                            session_type = fh_log["type"].split("_")[0]
                            fh_sessions[session_type] = fh_log["time"]
                        if "END" in fh_log["type"] or "PAUSE" in fh_log["type"] or "STOP" in fh_log["type"]:
                            session_type = fh_log["type"].split("_")[0]
                            if session_type in sessions:
                                duration = ((fh_log["time"] - fh_sessions[session_type]) / 1000)
                                first_half_total_duration += duration
                stats_sheet.append([first_half_total_duration])

                stats_sheet.append([])
                stats_sheet.append([f"Duration {second_half}"])
                second_half_total_duration = 0
                sh_sessions = {}
                for sh_log in second_half_logs:
                    if sh_log["type"] in Activities.__members__:
                        if "START" in sh_log["type"] or "RESUME" in sh_log["type"]:
                            session_type = sh_log["type"].split("_")[0]
                            sh_sessions[session_type] = sh_log["time"]
                        if "END" in sh_log["type"] or "PAUSE" in sh_log["type"] or "STOP" in sh_log["type"]:
                            session_type = sh_log["type"].split("_")[0]
                            if session_type in sessions:
                                duration = ((sh_log["time"] - sh_sessions[session_type]) / 1000)
                                second_half_total_duration += duration
                stats_sheet.append([second_half_total_duration])
            wb.save(f"log_exports/{user['participantId']}.xlsx")
        wb.close()


def export_user_logs_as_csv():
    users = db.get_all_users()
    with open('study_datapoints.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',')
        csv_writer.writerow(["Total Duration Rand", "Total Duration Pers", "Article Duration Rand",
                             "Article Duration Pers", "Book Duration Rand", "Book Duration Pers",
                             "Podcast Duration Rand", "Podcast Duration Pers", "App Duration Rand",
                             "App Duration Pers", "Article Clicks Rand", "Article Clicks Pers",
                             "Book Clicks Rand", "Book Clicks Pers", "Podcast Clicks Rand",
                             "Podcast Clicks Pers", "Total Ntfcn Clicks Rand", "Total Ntfcn Clicks Pers",
                             "Article Ntfcn Clicks Rand", "Article Ntfcn Clicks Pers", "Book Ntfcn Clicks Rand",
                             "Book Ntfcn Clicks Pers", "Podcast Ntfcn Clicks Rand", "Podcast Ntfcn Clicks Pers",
                             "Flashcard Ntfctn Clicks Rand", "Flashcard Ntfcn Clicks Pers",
                             "Cleaned Reader Clicks Rand", "Cleaned Reader Clicks Pers",
                             "Translated Reader Clicks Rand", "Translated Reader Clicks Pers",
                             "Dictionary Clicks Rand", "Dictionary Clicks Pers", "Favorites Rand", "Favorites Pers"])
        for user in users:
            if "logs" in user and "personalised" in user and "switch_time" in user:
                logs = user["logs"]
                sorted_logs = sorted(logs, key=lambda i: i['time'])
                sorted_logs = _remove_duplictes(sorted_logs)
                sessions = {}
                # first half
                fh_total_duration = 0
                fh_article_duration = 0
                fh_book_duration = 0
                fh_podcast_duration = 0
                fh_app_duration = 0
                fh_article_clicks = 0
                fh_book_clicks = 0
                fh_podcast_clicks = 0
                fh_total_notification_clicks = 0
                fh_article_notification_clicks = 0
                fh_book_notification_clicks = 0
                fh_podcast_notification_clicks = 0
                fh_flashcard_notification_clicks = 0
                fh_cleaned_reader_clicks = 0
                fh_translated_reader_clicks = 0
                fh_dictionary_clicks = 0
                fh_add_favorite = 0
                # second half
                sh_total_duration = 0
                sh_article_duration = 0
                sh_book_duration = 0
                sh_podcast_duration = 0
                sh_app_duration = 0
                sh_article_clicks = 0
                sh_book_clicks = 0
                sh_podcast_clicks = 0
                sh_total_notification_clicks = 0
                sh_article_notification_clicks = 0
                sh_book_notification_clicks = 0
                sh_podcast_notification_clicks = 0
                sh_flashcard_notification_clicks = 0
                sh_cleaned_reader_clicks = 0
                sh_translated_reader_clicks = 0
                sh_dictionary_clicks = 0
                sh_add_favorite = 0
                for log in sorted_logs:
                    if log["type"] in Activities.__members__ and "time" in log:
                        if "START" in log["type"] or "RESUME" in log["type"]:
                            session_type = log["type"].split("_")[0]
                            sessions[session_type] = log["time"]
                            # handle clicks
                            if log["time"] < user["switch_time"]:
                                if "ARTICLE" in session_type:
                                    fh_article_clicks += 1
                                elif "BOOK" in session_type:
                                    fh_book_clicks += 1
                                elif "PODCAST" in session_type:
                                    fh_podcast_clicks += 1
                            elif log["time"] < user["switch_time"] + (3 * 7 * 24 * 60 * 60 * 1000):  # 3 weeks
                                if "ARTICLE" in session_type:
                                    sh_article_clicks += 1
                                elif "BOOK" in session_type:
                                    sh_book_clicks += 1
                                elif "PODCAST" in session_type:
                                    sh_podcast_clicks += 1
                        if "END" in log["type"] or "PAUSE" in log["type"] or "STOP" in log["type"]:
                            session_type = log["type"].split("_")[0]
                            if session_type in sessions:
                                duration = ((log["time"] - sessions[session_type]) / 1000)
                                # handle sessions
                                if log["time"] < user["switch_time"]:
                                    fh_total_duration += duration
                                    if "ARTICLE" in session_type:
                                        fh_article_duration += duration
                                    elif "BOOK" in session_type:
                                        fh_book_duration += duration
                                    elif "PODCAST" in session_type:
                                        fh_podcast_duration += duration
                                    elif "APP" in session_type:
                                        fh_app_duration += duration
                                elif log["time"] < user["switch_time"] + (3 * 7 * 24 * 60 * 60 * 1000):  # 3 weeks
                                    sh_total_duration += duration
                                    if "ARTICLE" in session_type:
                                        sh_article_duration += duration
                                    elif "BOOK" in session_type:
                                        sh_book_duration += duration
                                    elif "PODCAST" in session_type:
                                        sh_podcast_duration += duration
                                    elif "APP" in session_type:
                                        sh_app_duration += duration
                    elif log["type"] in Notifications.__members__ and "time" in log:
                        if log["time"] < user["switch_time"]:
                            if "CLICK" in log["type"]:
                                fh_total_notification_clicks += 1
                                if "ARTICLE" in log["type"]:
                                    fh_article_notification_clicks += 1
                                elif "BOOK" in log["type"]:
                                    fh_book_notification_clicks += 1
                                elif "PODCAST" in log["type"]:
                                    fh_podcast_notification_clicks += 1
                                elif "FLASHCARD" in log["type"]:
                                    fh_flashcard_notification_clicks += 1
                        elif log["time"] < user["switch_time"] + (3 * 7 * 24 * 60 * 60 * 1000):  # 3 weeks
                            if "CLICK" in log["type"]:
                                sh_total_notification_clicks += 1
                                if "ARTICLE" in log["type"]:
                                    sh_article_notification_clicks += 1
                                elif "BOOK" in log["type"]:
                                    sh_book_notification_clicks += 1
                                elif "PODCAST" in log["type"]:
                                    sh_podcast_notification_clicks += 1
                                elif "FLASHCARD" in log["type"]:
                                    sh_flashcard_notification_clicks += 1
                    if "SIMPLE_READER_CLICK" in log["type"]:
                        if log["time"] < user["switch_time"]:
                            fh_cleaned_reader_clicks += 1
                        elif log["time"] < user["switch_time"] + (3 * 7 * 24 * 60 * 60 * 1000):  # 3 weeks
                            sh_cleaned_reader_clicks += 1
                    elif "PARAGRAPH_READER_CLICK" in log["type"]:
                        if log["time"] < user["switch_time"]:
                            fh_translated_reader_clicks += 1
                        elif log["time"] < user["switch_time"] + (3 * 7 * 24 * 60 * 60 * 1000):  # 3 weeks
                            sh_translated_reader_clicks += 1
                    elif "TRANSLATOR_CLICK" in log["type"]:
                        if log["time"] < user["switch_time"]:
                            fh_dictionary_clicks += 1
                        elif log["time"] < user["switch_time"] + (3 * 7 * 24 * 60 * 60 * 1000):  # 3 weeks
                            sh_dictionary_clicks += 1
                    elif "ADD_FAVORITE" in log["type"]:
                        if log["time"] < user["switch_time"]:
                            fh_add_favorite += 1
                        elif log["time"] < user["switch_time"] + (3 * 7 * 24 * 60 * 60 * 1000):  # 3 weeks
                            sh_add_favorite += 1
                if "personalised" in user:
                    if user["personalised"]:
                        csv_writer.writerow([fh_total_duration, sh_total_duration, fh_article_duration,
                                             sh_article_duration, fh_book_duration, sh_book_duration,
                                             fh_podcast_duration, sh_podcast_duration, fh_app_duration,
                                             sh_app_duration, fh_article_clicks, sh_article_clicks,
                                             fh_book_clicks, sh_book_clicks, fh_podcast_clicks,
                                             sh_podcast_clicks, fh_total_notification_clicks,
                                             sh_total_notification_clicks, fh_article_notification_clicks,
                                             sh_article_notification_clicks, fh_book_notification_clicks,
                                             sh_book_notification_clicks, fh_podcast_notification_clicks,
                                             sh_podcast_notification_clicks, fh_flashcard_notification_clicks,
                                             sh_flashcard_notification_clicks, fh_cleaned_reader_clicks,
                                             sh_cleaned_reader_clicks, fh_translated_reader_clicks,
                                             sh_translated_reader_clicks, fh_dictionary_clicks, sh_dictionary_clicks,
                                             fh_add_favorite, sh_add_favorite])
                    else:
                        csv_writer.writerow([sh_total_duration, fh_total_duration, sh_article_duration,
                                             fh_article_duration, sh_book_duration, fh_book_duration,
                                             sh_podcast_duration, fh_podcast_duration, sh_app_duration,
                                             fh_app_duration, sh_article_clicks, fh_article_clicks,
                                             sh_book_clicks, fh_book_clicks, sh_podcast_clicks,
                                             fh_podcast_clicks, sh_total_notification_clicks,
                                             fh_total_notification_clicks, sh_article_notification_clicks,
                                             fh_article_notification_clicks, sh_book_notification_clicks,
                                             fh_book_notification_clicks, sh_podcast_notification_clicks,
                                             fh_podcast_notification_clicks, sh_flashcard_notification_clicks,
                                             fh_flashcard_notification_clicks, sh_cleaned_reader_clicks,
                                             fh_cleaned_reader_clicks, sh_translated_reader_clicks,
                                             fh_translated_reader_clicks, sh_dictionary_clicks, fh_dictionary_clicks,
                                             sh_add_favorite, fh_add_favorite])


def export_daily_usage():
    users = db.get_all_users()
    with open('study_dailyusage.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',')
        csv_writer.writerow(["user", "day", "time spent", "interactions", "personalised"])
        user_counter = 0
        for user in users:
            if "logs" in user and "personalised" in user and "switch_time" in user:
                logs = user["logs"]
                sorted_logs = sorted(logs, key=lambda i: i['time'])
                sorted_logs = _remove_duplictes(sorted_logs)
                sessions = {}
                first_log_time = sorted_logs[0]["time"] / 1000  # from millis to seconds
                days_since_epoch = int(first_log_time / 86400)
                start_days_since_epoch = days_since_epoch
                user_data_time = [0] * 42
                user_data_clicks = [0] * 42
                personalised_list = [not user["personalised"]] * 21 + [user["personalised"]] * 21
                for log in sorted_logs:
                    if log["type"] in Activities.__members__ and "time" in log:
                        if (int((log["time"] / 1000) / 86400) - start_days_since_epoch) >= 42:
                            break
                        if "START" in log["type"] or "RESUME" in log["type"]:
                            session_type = log["type"].split("_")[0]
                            sessions[session_type] = log["time"]
                            if days_since_epoch < int((log["time"] / 1000) / 86400):
                                days_since_epoch = int((log["time"] / 1000) / 86400)
                            user_data_clicks[(days_since_epoch - start_days_since_epoch)] += 1
                        if "END" in log["type"] or "PAUSE" in log["type"] or "STOP" in log["type"]:
                            session_type = log["type"].split("_")[0]
                            if session_type in sessions:
                                duration = ((log["time"] - sessions[session_type]) / 1000)
                                if days_since_epoch < int((log["time"] / 1000) / 86400):
                                    days_since_epoch = int((log["time"] / 1000) / 86400)
                                user_data_time[(days_since_epoch - start_days_since_epoch)] += duration
                        elif ("CLICK" in log["type"] or "ADD_FAVORITE" in log["type"]
                              or "REMOVE_FAVORITE" in log["type"] or "NOTIFICATION_SET" in log["type"]
                              or "SWIPE_TO_REFRESH" in log["type"] or "ADD_WORD_OR_PHRASE" in log["type"]) \
                                and "time" in log:
                            if days_since_epoch < int((log["time"] / 1000) / 86400):
                                days_since_epoch = int((log["time"] / 1000) / 86400)
                            user_data_clicks[(days_since_epoch - start_days_since_epoch)] += 1

                for day_index in range(0, 42):
                    csv_writer.writerow([user_counter + 1, day_index + 1, user_data_time[day_index],
                                         user_data_clicks[day_index], personalised_list[day_index]])
                user_counter += 1



def _remove_duplictes(duplicates: list) -> list:
    seen = set()
    new_l = []
    for d in duplicates:
        t = tuple(sorted(d.items()))
        if t not in seen:
            seen.add(t)
            new_l.append(d)
    return new_l


if __name__ == "__main__":
    export_daily_usage()
    export_user_logs_as_csv()
