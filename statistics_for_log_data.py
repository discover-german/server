import csv
import inspect
from copy import copy


def get_statistics_from_log_data():
    total_duration_rand = []
    total_duration_pers = []
    article_duration_rand = []
    article_duration_pers = []
    book_duration_rand = []
    book_duration_pers = []
    podcast_duration_rand = []
    podcast_duration_pers = []
    app_duration_rand = []
    app_duration_pers = []
    article_clicks_rand = []
    article_clicks_pers = []
    book_clicks_rand = []
    book_clicks_pers = []
    podcast_clicks_rand = []
    podcast_clicks_pers = []
    total_notification_clicks_rand = []
    total_notification_clicks_pers = []
    article_notification_clicks_rand = []
    article_notification_clicks_pers = []
    book_notification_clicks_rand = []
    book_notification_clicks_pers = []
    podcast_notification_clicks_rand = []
    podcast_notification_clicks_pers = []
    flashcard_notification_clicks_rand = []
    flashcard_notification_clicks_pers = []
    cleaned_reader_clicks_rand = []
    cleaned_reader_clicks_pers = []
    translated_reader_clicks_rand = []
    translated_reader_clicks_pers = []
    dictionary_clicks_rand = []
    dictionary_clicks_pers = []
    favorites_rand = []
    favorites_pers = []

    with open("study_datapoints.csv", "r") as sd:
        reader = csv.reader(sd)
        for r_line in reader:
            line = copy(r_line)
            try:
                line = [float(x) for x in line]
            except Exception as e:
                continue
            total_duration_rand.append(line[0])
            total_duration_pers.append(line[1])
            article_duration_rand.append(line[2])
            article_duration_pers.append(line[3])
            book_duration_rand.append(line[4])
            book_duration_pers.append(line[5])
            podcast_duration_rand.append(line[6])
            podcast_duration_pers.append(line[7])
            app_duration_rand.append(line[8])
            app_duration_pers.append(line[9])
            article_clicks_rand.append(line[10])
            article_clicks_pers.append(line[11])
            book_clicks_rand.append(line[12])
            book_clicks_pers.append(line[13])
            podcast_clicks_rand.append(line[14])
            podcast_clicks_pers.append(line[15])
            total_notification_clicks_rand.append(line[16])
            total_notification_clicks_pers.append(line[17])
            article_notification_clicks_rand.append(line[18])
            article_notification_clicks_pers.append(line[19])
            book_notification_clicks_rand.append(line[20])
            book_notification_clicks_pers.append(line[21])
            podcast_notification_clicks_rand.append(line[22])
            podcast_notification_clicks_pers.append(line[23])
            flashcard_notification_clicks_rand.append(line[24])
            flashcard_notification_clicks_pers.append(line[25])
            cleaned_reader_clicks_rand.append(line[26])
            cleaned_reader_clicks_pers.append(line[27])
            translated_reader_clicks_rand.append(line[28])
            translated_reader_clicks_pers.append(line[29])
            dictionary_clicks_rand.append(line[30])
            dictionary_clicks_pers.append(line[31])
            favorites_rand.append(line[32])
            favorites_pers.append(line[33])

        all_measures = [total_duration_rand,
                        total_duration_pers,
                        article_duration_rand,
                        article_duration_pers,
                        book_duration_rand,
                        book_duration_pers,
                        podcast_duration_rand,
                        podcast_duration_pers,
                        app_duration_rand,
                        app_duration_pers,
                        article_clicks_rand,
                        article_clicks_pers,
                        book_clicks_rand,
                        book_clicks_pers,
                        podcast_clicks_rand,
                        podcast_clicks_pers,
                        total_notification_clicks_rand,
                        total_notification_clicks_pers,
                        article_notification_clicks_rand,
                        article_notification_clicks_pers,
                        book_notification_clicks_rand,
                        book_notification_clicks_pers,
                        podcast_notification_clicks_rand,
                        podcast_notification_clicks_pers,
                        flashcard_notification_clicks_rand,
                        flashcard_notification_clicks_pers,
                        cleaned_reader_clicks_rand,
                        cleaned_reader_clicks_pers,
                        translated_reader_clicks_rand,
                        translated_reader_clicks_pers,
                        dictionary_clicks_rand,
                        dictionary_clicks_pers,
                        favorites_rand,
                        favorites_pers]

        for measure in all_measures:
            mean = find_mean(measure)
            var, sd = find_variance(measure, 'sample')
            print(f"{retrieve_name(measure)[0]} {mean:.2f}, {sd:.2f}")

def retrieve_name(var):
    callers_local_vars = inspect.currentframe().f_back.f_locals.items()
    return [var_name for var_name, var_val in callers_local_vars if var_val is var]


def find_variance(list_of_numbers, var_type):
    # argument check
    valid_var_types = ['sample', 'population']
    if var_type not in valid_var_types:
        raise ValueError(
            f'Incorrect argument for variance type passed: {var_type}! Allowed values \'sample\' or \'population\'')
    # define correct sequence length
    seq_length = len(list_of_numbers) - 1 if var_type == 'sample' else len(list_of_numbers)
    # using the previously defined function
    mean = find_mean(list_of_numbers)
    # find differences and squared differences
    differences = [number - mean for number in list_of_numbers]
    squared_differences = [x ** 2 for x in differences]
    # calculate
    variance = sum(squared_differences) / seq_length
    std = variance ** 0.5
    return round(variance, 2), round(std, 2)


def find_mean(list_of_numbers):
    sum_n = sum(list_of_numbers)
    len_n = len(list_of_numbers)
    mean = sum_n / len_n
    return mean


if __name__ == "__main__":
    get_statistics_from_log_data()
