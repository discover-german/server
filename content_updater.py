import os
import time
import copy
import feedparser
from pymongo.errors import DocumentTooLarge
from database import Database
from string_processing import StringProcessing
from recommender import Recommender
from crawler import aggregate_all_articles
from scripts.podcast_rss_crawler import extract_entries_from_feed
from constants import ARTICLES_HALFLIFE, PODCASTS_HALFLIFE

class ContentUpdater:
    db = Database()
    rc = Recommender()
    sp = StringProcessing()

    def update_articles(self):
        now = time.time()
        articles = self.db.get_all_articles()
        users = self.db.get_all_users()
        articles_to_delete = []
        for user in users:
            u_oid = user["_id"]
            for article in articles:
                if article["added"] < (now - 2*ARTICLES_HALFLIFE):
                    if article not in articles_to_delete:
                        articles_to_delete.append(article)
                    self.db.delete_interest(article["_id"], "article", u_oid)
        a = 0
        for article_to_delete in articles_to_delete:
            self.db.save_article_to_archive(article_to_delete)
            self.db.delete_article(article_to_delete["_id"])
            self.db.delete_item_from_similar_items(article_to_delete["_id"], "article")
            a += 1
        print(f"deleted {a} articles")
        print(f"deleting old articles took: {time.time()-now}")
        aggregate_start = time.time()
        aggregate_all_articles()
        print(f"aggregation of new articles took {time.time()-aggregate_start}")
        project_folder = os.getcwd()
        for filename in os.listdir(project_folder+"/models"):
            if "article" in filename:
                os.remove(project_folder+"/models/"+filename)
        train_start = time.time()
        self.rc.train_lda("article", [30])
        print(f"training took {time.time()-train_start}")
        compare_start = time.time()
        self.rc.compare_all_articles(30)
        print(f"comparison took: {time.time()-compare_start}")
        return

    def update_podcasts(self):
        now = time.time()
        users = self.db.get_all_users()
        for user in users:
            new_user = copy.deepcopy(user)
            updated_interests = []
            if not "interests" in user:
                continue
            for interest in user["interests"]:
                if interest["added"] > (now - PODCASTS_HALFLIFE):
                    updated_interests.append(interest)
            new_user["interests"] = updated_interests
            self.db.replace_user(user, new_user)
        crawl_start = time.time()
        print(f"deleting podcast interests took {crawl_start-now}")
        new_content = self._crawl_new_episodes()
        train_start = time.time()
        # print(f"cleaning took: {train_start-clean_start}")
        self.rc.update_lda("podcast", [100], new_content)
        compare_start = time.time()
        print(f"training took: {compare_start-train_start}")
        self.rc.compare_all_podcasts(100)
        print(f"comparison took: {time.time()-compare_start}")

    def _crawl_new_episodes(self) -> list:
        crawl_start = time.time()
        podcasts = self.db.get_all_podcasts()
        update_start = time.time()
        print(f"retrieving old podcasts took {update_start - crawl_start}")
        updated_podcasts = []
        total_new_entries = 0
        for podcast in podcasts:
            updated_podcast = copy.deepcopy(podcast)
            if "rss" in podcast:
                rss_feed = feedparser.parse(podcast["rss"])
                new_entries_count = 0
                for reverse_index in range(0, len(podcast["entries"])):
                    new_entries_count = 0
                    for entry in rss_feed.entries:
                        if "title" in entry and entry["title"] == podcast["entries"][-(1 + reverse_index)]["title"]:
                            break
                        else:
                            new_entries_count += 1
                    else:
                        print(f"\rcould not find {-(1 + reverse_index)} episode of podcast in new episodes", end="")
                        continue
                    # cut the reverse_index amount of entries from the end of the list
                    if reverse_index > 0:
                        print(f"\nsynced episodes. had to remove {reverse_index} entries at the end of the list")
                    updated_podcast["entries"] = \
                        updated_podcast["entries"][:(len(updated_podcast["entries"]) - reverse_index)]
                    break
                else:
                    print("\ncould not sync episodes. appending new ones and assuming gap")
                if new_entries_count > 0:
                    total_new_entries += new_entries_count
                    image_link = ""
                    if "image" in rss_feed.feed:
                        if "href" in rss_feed.feed.image:
                            image_link = rss_feed.feed.image.href
                        elif "url" in rss_feed.feed.image:
                            print("found url")
                            image_link = rss_feed.feed.image.url
                    new_entries = rss_feed.entries[:new_entries_count]
                    new_entries_parsed = extract_entries_from_feed(new_entries, image_link)
                    updated_podcast["entries"].extend(new_entries_parsed)
            updated_podcasts.append(updated_podcast)
        clean_start = time.time()
        print(f"total number of new entries: {total_new_entries}")
        print(f"updating took {clean_start - update_start}")
        new_content = []
        for podcast, updated_podcast in zip(podcasts, updated_podcasts):
            podcast_entry_size = len(podcast["entries"])
            updated_podcast_entry_size = len(updated_podcast["entries"])
            if updated_podcast_entry_size > podcast_entry_size:
                new_cleaned_episodes = []
                for episode in updated_podcast["entries"][podcast_entry_size:]:
                    cleaned_text = self.sp.get_cleaned_text_for_episode(episode)
                    new_cleaned_episodes.append({"cleanedText": cleaned_text})
                if new_cleaned_episodes:
                    try:
                        cleaned_podcast = self.db.get_cleaned_podcast_by_original_id(podcast["_id"])
                        new_cleaned_podcast = copy.deepcopy(cleaned_podcast)
                        new_cleaned_podcast["cleanedEpisodes"].extend(new_cleaned_episodes)
                        if updated_podcast_entry_size == len(new_cleaned_podcast["cleanedEpisodes"]):
                            self.db.replace_podcast(podcast, updated_podcast)
                            self.db.replace_cleaned_podcast(cleaned_podcast, new_cleaned_podcast)
                            i = podcast_entry_size
                            for c_episode in new_cleaned_episodes:
                                new_content.append(
                                    {"raw": "", "cleaned": c_episode["cleanedText"], "oid": podcast["_id"], "index": i})
                                i += 1
                        else:
                            print(f"podcast episode size was unequal to cleaned podcast for {podcast['_id']}")
                    except DocumentTooLarge as e:
                        print(f"Document for podcast {podcast['title']} too large: {e}")
            elif updated_podcast_entry_size < podcast_entry_size:
                print("updated podcast entries are less than old entries. recalculating cleaned podcast entries")
                new_cleaned_episodes = []
                for episode in updated_podcast["entries"]:
                    cleaned_text = self.sp.get_cleaned_text_for_episode(episode)
                    new_cleaned_episodes.append({"cleanedText": cleaned_text})
                if new_cleaned_episodes:
                    try:
                        cleaned_podcast = self.db.get_cleaned_podcast_by_original_id(podcast["_id"])
                        new_cleaned_podcast = copy.deepcopy(cleaned_podcast)
                        new_cleaned_podcast["cleanedEpisodes"] = new_cleaned_episodes
                        self.db.replace_podcast(podcast, updated_podcast)
                        self.db.replace_cleaned_podcast(cleaned_podcast, new_cleaned_podcast)
                        i = 0
                        for c_episode in new_cleaned_episodes:
                            new_content.append(
                                {"raw": "", "cleaned": c_episode["cleanedText"], "oid": podcast["_id"], "index": i})
                            i += 1
                    except DocumentTooLarge as e:
                        print(f"Document for podcast {podcast['title']} too large: {e}")

        return new_content

    def _annotate_ct(self, text) -> dict:
        return {"cleanedText": text}


if __name__ == "__main__":
    cu = ContentUpdater()
    cu.update_articles()
    cu.update_podcasts()
