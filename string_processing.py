import re
import os
import string
import spacy
import json
import nltk
from nltk.corpus import stopwords
from database import Database

#nltk.download('stopwords')

nlp = spacy.load('de_core_news_lg')
with open(os.getcwd()+"/stop_words_german.json", "r") as stop_words_file:
    german_stop_words = json.load(stop_words_file)


class StringProcessing:

    def convert_params_to_list(self, params: string) -> list:
        if params:
            return params.split(",")
        else:
            return ""

    def remove_nonsense(self, text: str) -> str:
        text = text.lower()
        text = re.sub(r"([a-zÀ-ſ]{3,7}/)+[a-zÀ-ſ]{3,7}", "", text)
        text = re.sub(r"[^a-zÀ-ſ]+", " ", text)
        text = re.sub(r" +", " ", text)
        text = re.sub(r"dpa", "", text)
        return text

    def clean_text_for_nlp(self, text: string, filter_nouns=False) -> string:
        text = self.remove_nonsense(text)
        if len(text) > nlp.max_length:
            nlp.max_length = len(text) + 1
        text_nlp = nlp(text)
        words = [x.lemma_ for x in text_nlp]
        text = " ".join(word for word in words if word not in german_stop_words)
        text = " ".join(word for word in text.split() if word not in stopwords.words('german'))
        text = " ".join(word for word in text.split() if word not in stopwords.words('english'))
        if filter_nouns:
            text = self._filter_nouns(text)
        return text

    def _filter_nouns(self, text: string) -> string:
        text_nlp = nlp(text)
        word_pairs = [(x.lemma_, x.tag_) for x in text_nlp]
        words = []
        for word_pair in word_pairs:
            word_type = word_pair[1]
            if word_type == "NN" or word_type == "NE" or word_type == "NNE":  # \
                # or word_pair[1] == "VVFIN" or word_pair[1] == "VAFIN":
                words.append(word_pair[0])
        return " ".join(words)

    def calculate_readability_index(self, text: str) -> float:
        sentence_count = 0
        for line in text.split("\n"):
            if line != "":
                sentences = line.split(".")
                sentence_count += len(sentences)
        words = self.remove_nonsense(text).split()
        word_count = len(words)
        long_word_count = 0
        for word in words:
            if len(word) > 6:
                long_word_count += 1
        if long_word_count != 0:
            return (word_count / sentence_count) + (100 * long_word_count / word_count)
        else:
            return 0

    def get_difficulty_from_readability_index(self, readability_index: float) -> str:
        if readability_index > 60:
            difficulty = "C2"
        elif readability_index > 50:
            difficulty = "C1"
        elif readability_index > 40:
            difficulty = "B2"
        elif readability_index > 20:
            difficulty = "B1"
        else:
            difficulty = "A1/A2"
        return difficulty

    def bigram_extractor(self, bodies: list) -> list:
        bigram_measures = nltk.collocations.BigramAssocMeasures()
        most_pop_bigram_scores = self._bigram_filter(bodies, bigram_measures)
        most_popular_bigrams = [bigram_score[0] for bigram_score in most_pop_bigram_scores]
        all_documents_bigrams = []
        for body in bodies:
            document_bigrams = []
            for bigram in nltk.bigrams(body.split()):
                if bigram in most_popular_bigrams:  # and self._ngram_contains_noun(bigram):
                    document_bigrams.append(" ".join(bigram).lower())
            all_documents_bigrams.append(document_bigrams)
        return all_documents_bigrams

    def _bigram_filter(self, clean_content, bigram_measures):
        finder = nltk.collocations.BigramCollocationFinder \
            .from_documents([comment.split() for comment in clean_content])
        finder.apply_freq_filter(len(clean_content)/30)
        bigram_scores = finder.score_ngrams(bigram_measures.pmi)
        return bigram_scores

    def trigram_extractor(self, bodies: list) -> list:
        trigram_measures = nltk.collocations.TrigramAssocMeasures()
        most_pop_trigram_scores = self._trigram_filter(bodies, trigram_measures)
        most_popular_trigrams = [trigram_score[0] for trigram_score in most_pop_trigram_scores]
        all_documents_trigrams = []
        for body in bodies:
            document_trigrams = []
            for trigram in nltk.trigrams(body.split()):
                if trigram in most_popular_trigrams:  # and self._ngram_contains_noun(trigram):
                    document_trigrams.append(" ".join(trigram).lower())
            all_documents_trigrams.append(document_trigrams)
        return all_documents_trigrams

    def _trigram_filter(self, clean_content, trigram_measures):
        finder = nltk.collocations.TrigramCollocationFinder \
            .from_documents([comment.split() for comment in clean_content])
        finder.apply_freq_filter(len(clean_content) / 50)
        trigram_scores = finder.score_ngrams(trigram_measures.pmi)
        return trigram_scores

    def _ngram_contains_noun(self, ngram: iter) -> bool:
        for part in ngram:
            part_nlp = nlp(part)
            if part_nlp[0].tag_ in ["NN", "NE", "NNE"]:
                return True
        return False

    def merge_tokens_with_bigrams(self, all_tokens: list, all_bigrams: list) -> list:
        merged_documents = []
        if len(all_tokens) == len(all_bigrams):
            for tokens, bigrams in zip(all_tokens, all_bigrams):
                merged_doc = []
                first_elements = [bigram.split()[0] for bigram in bigrams]
                second_elements = [bigram.split()[1] for bigram in bigrams]
                for token_index in range(0, len(tokens)):
                    if (tokens[token_index] in first_elements and token_index + 1 < len(tokens)
                        and tokens[token_index + 1] in second_elements) or \
                            (token_index > 0 and tokens[token_index - 1] in first_elements and
                             tokens[token_index] in second_elements):
                        continue
                    else:
                        merged_doc.append(tokens[token_index])
                merged_doc.extend(bigrams)
                merged_documents.append(merged_doc)
        return merged_documents

    def get_cleaned_text_for_episode(self, episode: dict) -> str:
        cleaned_title = ""
        cleaned_description = ""
        cleaned_summary = ""
        cleaned_keywords = ""
        if episode["title"] is not None:
            cleaned_title = self.clean_text_for_nlp(episode["title"])
        if episode["description"] is not None:
            cleaned_description = self.clean_text_for_nlp(episode["description"])
        if episode["summary"] is not None:
            cleaned_summary = self.clean_text_for_nlp(episode["summary"])
        if episode["keywords"] is not None:
            cleaned_keywords = self.clean_text_for_nlp(episode["keywords"])
        return self.merge_podcast_text(cleaned_title, cleaned_description, cleaned_summary, cleaned_keywords)

    def merge_podcast_text(self, title, description, summary, keywords):
        cleaned_text = ""
        cleaned_text += title + " "
        if description not in cleaned_text:
            cleaned_text += description + " "
        if summary not in cleaned_text:
            cleaned_text += summary + " "
        if keywords not in cleaned_text:
            cleaned_text += keywords + " "
        return cleaned_text.strip()

if __name__ == "__main__":
    sp = StringProcessing()
    db = Database()
    oid = "61e19a99a31bac6ddc4c0b07"
    a = db.get_item(oid, "article")
    ri = sp.calculate_readability_index(a["body"])
    print(ri)
