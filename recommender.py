import copy
import logging
import os
import random
import time
import pickle

import numpy as np
# import pyLDAvis
# from pyLDAvis import gensim
from database import Database
from string_processing import StringProcessing
from gensim import corpora, models, matutils
# import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from multiprocessing import Pool
from itertools import repeat
from bson import ObjectId

db = Database()
sp = StringProcessing()

NO_BELOW = 5
NO_ABOVE = 0.5


class Recommender:
    tfidf_vectorizer = TfidfVectorizer()
    p_corpus_lda_model = None
    p_tfidf = None
    p_content = None
    p_lda_model_final = None

    def get_recommendations(self, participant_id: str, blacklist: list, cutoff: str, difficulty: str):
        if type(cutoff) == str:
            cutoff = int(cutoff)
        if db.user_exists(participant_id):
            try:
                start = time.time()
                articles = self._get_article_recommendations(participant_id, blacklist, cutoff, difficulty)
                books = self._get_book_recommendations(participant_id, blacklist, cutoff, difficulty)
                podcasts = self._get_podcast_recommendations(participant_id, blacklist, cutoff, difficulty)
                print(f"time for recommendation: {time.time() - start}")
                return {"articles": articles, "books": books, "podcasts": podcasts}
            except Exception as e:
                print(e)
        else:
            return {}

    def get_personalised_item(self, participant_id: str, content_type: str, blacklist: list, difficulty: str) -> dict:
        interests = db.get_interests(participant_id, 1, content_type)
        if interests:
            dismissed = db.get_dismissed(participant_id)
            if content_type == "article":
                recommendations = db.get_article_recommendations(interests, blacklist, dismissed, difficulty)
                item = [recommendations[0]["recommendation"]]
            elif content_type == "book":
                recommendations = db.get_book_recommendations(interests, dismissed, difficulty)
                item = [recommendations[0]["recommendation"]]
            elif content_type == "podcast":
                recommendations = db.get_podcast_recommendations(interests, dismissed, difficulty)
                item = [recommendations[0]["recommendation"]]
            else:
                item = db.get_parsed_item("", content_type)
        else:
            item = db.get_parsed_item("", content_type)
        return item

    def _get_article_recommendations(self, participant_id, blacklist, cutoff, difficulty) -> list:
        if "NEWS" not in blacklist:
            interests = db.get_interests(participant_id, cutoff, "article")
            dismissed = db.get_dismissed(participant_id)
            personalised = db.get_personalised(participant_id)
            if personalised:
                recommendations = db.get_article_recommendations(interests, blacklist, dismissed, difficulty)
            else:
                recommendations = []
            recommendation_ids = [x["recommendation"]["_id"] for x in recommendations]
            while len(recommendations) < 10:
                random_article = db.get_random_articles(1)[0]
                if random_article["_id"] not in recommendation_ids and random_article["source"] not in blacklist:
                    recommendations.append({"recommendation": random_article, "trigger": {"title": ""}})
            if personalised:
                random.shuffle(recommendations)
            return recommendations
        else:
            return []

    def _get_book_recommendations(self, participant_id, blacklist, cutoff, difficulty) -> list:
        if "BOOKS" not in blacklist:
            interests = db.get_interests(participant_id, cutoff, "book")
            dismissed = db.get_dismissed(participant_id)
            personalised = db.get_personalised(participant_id)
            if personalised:
                recommendations = db.get_book_recommendations(interests, dismissed, difficulty)
            else:
                recommendations = []
            recommendation_ids = [x["recommendation"]["_id"] for x in recommendations]
            while len(recommendations) < 10:
                random_book = db.get_random_books(1)[0]
                if random_book["_id"] not in recommendation_ids:
                    recommendations.append({"recommendation": random_book, "trigger": {"title": "", "text": []}})
            for rec in recommendations:
                if len(rec["recommendation"]["text"]) > 0:
                    rec["recommendation"]["text"] = rec["recommendation"]["text"][:1]
                if len(rec["trigger"]["text"]) > 0:
                    rec["trigger"]["text"] = rec["trigger"]["text"][:1]
            if personalised:
                random.shuffle(recommendations)
            return recommendations
        else:
            return []

    def _get_podcast_recommendations(self, participant_id, blacklist, cutoff, difficulty) -> list:
        if "PODCASTS" not in blacklist:
            interests = db.get_interests(participant_id, cutoff, "podcast")
            dismissed = db.get_dismissed(participant_id)
            personalised = db.get_personalised(participant_id)
            if personalised:
                recommendations = db.get_podcast_recommendations(interests, dismissed, difficulty)
            else:
                recommendations = []
            recommendation_ids = [x["recommendation"]["_id"] for x in recommendations]
            while len(recommendations) < 10:
                random_podcast = db.get_random_podcasts(1)[0]
                if random_podcast["_id"] not in recommendation_ids:
                    recommendations.append({"recommendation": random_podcast, "trigger": {"title": ""},
                                            "episodeIndex": random.randint(0, len(random_podcast["entries"]) - 1)})
            if personalised:
                random.shuffle(recommendations)
            return recommendations
        else:
            return []

    def train_lda(self, content_type: str, topicnums: list):
        logging.basicConfig(format='%(levelname)s : %(message)s', level=logging.INFO)
        logging.root.level = logging.INFO
        content = self._retrieve_content(content_type)
        print("fetched content")
        text_tokens = self._extract_tokens(content)
        print("extracted tokens")
        dictionary = corpora.Dictionary(text_tokens)
        dictionary.filter_extremes(no_below=NO_BELOW, no_above=NO_ABOVE)
        print("created dictionary")
        bow_corpus = [dictionary.doc2bow(text) for text in text_tokens]
        print("created corpus")
        project_folder = os.getcwd()
        ldamodels_bow = {}
        for i in topicnums:
            random.seed(42)
            if not os.path.exists(project_folder + '/models/ldamodels_bow_' + content_type + '_' + str(i) + '.lda'):
                ldamodels_bow[i] = models.LdaModel(bow_corpus, num_topics=i, random_state=42, update_every=1,
                                                   passes=50, iterations=500, chunksize=2000,
                                                   id2word=dictionary, eval_every=1)
                ldamodels_bow[i].save(project_folder + '/models/ldamodels_bow_' +
                                      content_type + '_' + str(i) + '.lda')
                print('ldamodels_bow_{}_{}.lda created.'.format(content_type, i))
            else:
                print('ldamodels_bow_{}_{}.lda already exists.'.format(content_type, i))
        lda_topics = {}
        for i in topicnums:
            lda_model = models.LdaModel.load(project_folder + '/models/ldamodels_bow_' +
                                             content_type + '_' + str(i) + '.lda')
            lda_topics_string = lda_model.show_topics(i)
            lda_topics[i] = ["".join([c if c.isalpha() else " " for c in topic[1]]).split() for topic in
                             lda_topics_string]

        pickle.dump(lda_topics, open(project_folder + '/models/pub_lda_bow_book_topics.pkl', 'wb'))
        return content

    def update_lda(self, content_type: str, topicnums: list, new_content: list):
        text_tokens = self._extract_tokens(new_content)
        print("extracted tokens")
        dictionary = corpora.Dictionary(text_tokens)
        dictionary.filter_extremes(no_below=NO_BELOW, no_above=NO_ABOVE)
        print("created dictionary")
        new_corpus = [dictionary.doc2bow(text) for text in text_tokens]
        print("created corpus")
        project_folder = os.getcwd()
        ldamodels_bow = {}
        lda_topics = {}
        try:
            for i in topicnums:
                random.seed(42)
                if not os.path.exists(project_folder + '/models/ldamodels_bow_' + content_type + '_' + str(i) + '.lda'):
                    ldamodels_bow[i] = models.LdaModel.load(project_folder + '/models/ldamodels_bow_' +
                                                            content_type + '_' + str(i) + '.lda')
                    ldamodels_bow[i] = ldamodels_bow[i].update(new_corpus)
                    lda_topics_string = ldamodels_bow[i].show_topics(i)
                    lda_topics[i] = ["".join([c if c.isalpha() else " " for c in topic[1]]).split() for topic in
                                     lda_topics_string]
                    ldamodels_bow[i].save(project_folder + '/models/ldamodels_bow_' +
                                          content_type + '_' + str(i) + '.lda')
                    print('ldamodels_bow_book_{}.lda updated.'.format(i))
            pickle.dump(lda_topics, open(project_folder + '/models/pub_lda_bow_book_topics.pkl', 'wb'))
        except Exception as e:
            print(f"could not update lda model: {e}")
        return

    def calculate_lda_stability(self):
        project_folder = os.getcwd()
        lda_topics = pickle.load(open(project_folder + '/models/pub_lda_bow_topics.pkl', 'rb'))
        lda_stability = {}
        for i in range(0, len(self.topicnums) - 1):
            jacc_sims = []
            for t1, topic1 in enumerate(lda_topics[self.topicnums[i]]):
                sims = []
                for t2, topic2 in enumerate(lda_topics[self.topicnums[i + 1]]):
                    sims.append(self._jaccard_similarity(topic1, topic2))
                jacc_sims.append(sims)
            lda_stability[self.topicnums[i]] = jacc_sims
        mean_stability = [np.array(lda_stability[i]).mean() for i in self.topicnums[:-1]]

        with sns.axes_style("darkgrid"):
            x = self.topicnums[:-1]
            y = mean_stability
            plt.figure(figsize=(20, 10))
            plt.plot(x, y, label='Average Overlap Between Topics')
            plt.xlim([1, 55])
            plt.ylim([0, 0.25])
            plt.xlabel('Number of topics')
            plt.ylabel('Average Jaccard similarity')
            plt.title('Average Jaccard Similarity Between Topics')
            plt.legend()
            plt.show()
        return

    def calculate_coherence_models(self, content_type: str):
        coherences = []
        project_folder = os.getcwd()
        content = self._retrieve_content(content_type)
        text_tokens = self._extract_tokens(content)
        dictionary = corpora.Dictionary(text_tokens)
        dictionary = dictionary.filter_extremes(no_below=NO_BELOW, no_above=NO_ABOVE)
        for i in self.topicnums:
            ldamodel = models.LdaModel.load(project_folder + '/models/ldamodels_bow_' + content_type
                                            + '_' + str(i) + '.lda')
            cm = models.coherencemodel.CoherenceModel(
                model=ldamodel, texts=text_tokens,
                dictionary=dictionary, coherence='c_v')

            coherences.append((i, cm.get_coherence()))
        with sns.axes_style("darkgrid"):
            x = self.topicnums
            y = [coherence[1] for coherence in coherences]
            plt.figure(figsize=(20, 10))
            plt.plot(x, y, label='Topic Coherence')
            plt.xlim([1, 45])
            plt.ylim([0, 1])
            plt.xlabel('Number of topics')
            plt.ylabel('Coherence')
            plt.title('Coherence of topics')
            plt.legend()
            plt.show()

    def _find_lda_recommendations(self, input_text: str, corpus_lda_model, all_sims, calculated_sims,
                                  content, lda_model_final, content_type="article", episode_index=None):
        cleaned_bodies = [item["cleaned"] for item in content]
        for i in range(len(cleaned_bodies)):
            recommendation_scores = []
            if cleaned_bodies[i] == input_text:
                # sims = al[lda_vectors]
                if i in calculated_sims:  # NAN means the sim was not calculated. we want the opposite here
                    sims = all_sims[i]
                    for item_num, sim in enumerate(sims):
                        if "difficulty" in content[item_num]:
                            if episode_index is not None:
                                recommendation_score = {"oid": content[item_num]["oid"], "score": float(sim),
                                                        "difficulty": content[item_num]["difficulty"],
                                                        "episodeIndex": content[item_num]["index"]}
                            else:
                                recommendation_score = {"oid": content[item_num]["oid"], "score": float(sim),
                                                        "difficulty": content[item_num]["difficulty"]}
                        else:
                            if episode_index is not None:
                                recommendation_score = {"oid": content[item_num]["oid"], "score": float(sim),
                                                        "episodeIndex": content[item_num]["index"]}
                            else:
                                recommendation_score = {"oid": content[item_num]["oid"], "score": float(sim)}
                        recommendation_scores.append(recommendation_score)
                else:
                    # Sim == None means the similarities have been calculated in the past. In this case, we infer
                    # updated similarity scores based on the symmetric distance between new items and the old one
                    recommendation_scores = self._infer_updated_recommendation_for_old_item(i, all_sims,
                                                                                            calculated_sims,
                                                                                            content, content_type,
                                                                                            episode_index)
                recommendation = sorted(recommendation_scores, key=lambda x: x["score"], reverse=True)
                try:
                    lda_vectors = corpus_lda_model[i]
                    if lda_vectors:
                        most_prominent_topics = lda_model_final.print_topic(max(lda_vectors, key=lambda item: item[1])[0])
                    else:
                        most_prominent_topics = []
                    return recommendation, most_prominent_topics
                except Exception as e:
                    logging.error("lda", f"could not find lda vectors:")
                    print(e)
        return [], []

    def _infer_updated_recommendation_for_old_item(self, index: int, all_sims: list, calculated_sims: list,
                                                   content: list, content_type: str, episode_index=None, ) -> list:
        updated_recommendation = []
        if index not in calculated_sims:  # NAN means the index was not calculated, because it already exists in db
            item = content[index]
            oid = item["oid"]
            old_sims = db.get_similarity(oid, content_type, episode_index)
            if old_sims is None:
                print(f"\nno sims found for {oid}, {episode_index}")
                return updated_recommendation
            updated_recommendation.extend(old_sims["scores"])
            for calculated_sim_index in calculated_sims:
                inner_sim = all_sims[calculated_sim_index]
                score = float(inner_sim[index])
                if episode_index is not None:
                    updated_recommendation.append({"oid": content[calculated_sim_index]["oid"], "score": score,
                                                   "episodeIndex": content[calculated_sim_index]["index"]})
                else:
                    updated_recommendation.append({"oid": content[calculated_sim_index]["oid"], "score": score})
        return updated_recommendation

    def compare_all_articles(self, num_topics=20):
        articles = db.get_all_articles()
        content = self._retrieve_content("article")
        corpus_lda_model, all_sims, \
        lda_model_final, calculated_sims = self._prepare_lda_for_comparison(num_topics, "article", content)
        for article in articles:
            self._calculate_similarities_for_article_and_store_in_db(article, corpus_lda_model, all_sims,
                                                                     calculated_sims, content, lda_model_final,
                                                                     "article")
        return

    def compare_all_books(self, num_topics=20):
        books = db.get_all_books()
        content = self._retrieve_content("book")
        cleaned_books = db.get_all_cleaned_books()
        corpus_lda_model, all_sims, lda_model_final, \
        calculated_sims = self._prepare_lda_for_comparison(num_topics, "book", content)
        print("finished calculating sims")
        index = 1
        for book, c_book in zip(books, cleaned_books):
            cleaned_text = " ".join(c_book["text"])
            self._calculate_similarities_for_book_and_store_in_db(book["_id"], cleaned_text, corpus_lda_model,
                                                                  all_sims, calculated_sims, content, lda_model_final,
                                                                  "book")
            print(f"\rstored sims for book number: {index}", end="")
            index += 1
        return

    def compare_all_podcasts(self, num_topics=50):
        content = self._retrieve_content("podcast")
        corpus_lda_model, all_sims, lda_model_final, \
            calculated_sims = self._prepare_lda_for_comparison(num_topics, "podcast", content)
        oids = [doc["oid"] for doc in content]
        episode_indices = [doc["index"] for doc in content]
        podcast_index = 0
        for oid, episode_index in zip(oids, episode_indices):
            if episode_index == 0:
                podcast_index += 1
            self._calculate_similarities_for_podcast_and_store_in_db(oid, episode_index, corpus_lda_model, all_sims,
                                                                     calculated_sims, content, lda_model_final,
                                                                     "podcast")
            print(f"\rcalculated sims for episode {episode_index} of podcast {podcast_index}", end="")
        return

    def _get_document_offsets(self) -> list:
        podcasts = db.get_all_podcasts()
        document_offsets = []
        offset = 0
        for podcast in podcasts:
            document_offsets.append(offset)
            offset += len(podcast["entries"])
        return document_offsets

    def _prepare_lda_for_comparison(self, num_topics, content_type, content=None):
        project_folder = os.getcwd()
        lda_model_final = models.LdaModel.load(project_folder + '/models/ldamodels_bow_' + content_type
                                               + '_' + str(num_topics) + '.lda')
        if content is None:
            content = self._retrieve_content(content_type)
        cleaned_bodies = [doc["cleaned"] for doc in content]
        oids = [doc["oid"] for doc in content]
        if content_type == "podcast":
            episode_indices = [doc["index"] for doc in content]
        else:
            episode_indices = None
        tfidf = self.tfidf_vectorizer.fit_transform(cleaned_bodies)
        text_tokens = self._extract_tokens(content)
        dictionary = corpora.Dictionary(text_tokens)
        dictionary.filter_extremes(no_below=NO_BELOW, no_above=NO_ABOVE)
        bow_corpus = [dictionary.doc2bow(text) for text in text_tokens]
        corpus_lda_model = lda_model_final[bow_corpus]
        all_sims, calculated_sims = self._calculate_all_tfidf_sims(tfidf, len(cleaned_bodies), oids, content_type,
                                                                   episode_indices)
        return corpus_lda_model, all_sims, lda_model_final, calculated_sims

    def _get_score_from_similarities(self, sims, oid) -> float:
        for sim in sims:
            if sim["oid"] == oid:
                return sim["score"]
        else:
            logging.error("similarity calculation", f"no similarity found for {oid}")
            return 0.

    def _calculate_all_hellinger_sims(self, corpus_lda_model):
        all_sims = []
        for vector in corpus_lda_model:
            sims = [(1 - matutils.hellinger(vector, comparison)) for comparison in corpus_lda_model]
            all_sims.append(sims)
        return all_sims

    def _calculate_all_cosine_sims(self, corpus_lda_model):
        all_sims = []
        for vector in corpus_lda_model:
            sims = [(matutils.cossim(vector, comparison)) for comparison in corpus_lda_model]
            all_sims.append(sims)
        return all_sims

    def _calculate_all_jensen_shannon_sims(self, corpus_lda_model, num_topics=30):
        all_sims = []
        for vector in corpus_lda_model:
            sims = [(1 - matutils.jensen_shannon(matutils.sparse2full(vector, num_topics),
                                                 matutils.sparse2full(comparison, num_topics)))
                    for comparison in corpus_lda_model]
            all_sims.append(sims)
        return all_sims

    def _calculate_all_tfidf_sims(self, tfidf, length, oids, content_type, episode_indices=None):
        all_sims = np.empty((length, length), dtype=np.float32)
        calculated_sims_indices = []
        for index, oid in zip(range(0, length), oids):
            if episode_indices is not None:
                episode_index = episode_indices[index]
                sim_exists = db.similarity_exists(oid, content_type, episode_index)
            else:
                sim_exists = db.similarity_exists(oid, content_type)
            if not sim_exists:
                cosine_similarities = self._calculate_tfidf_sims(tfidf, index)
                print(f"\rcalculated sims for {index} of {length}", end="")
                all_sims[index] = cosine_similarities
                calculated_sims_indices.append(index)
        return all_sims, calculated_sims_indices

    def _calculate_tfidf_sims(self, tfidf, index):
        return linear_kernel(tfidf[index:index + 1], tfidf).flatten()

    def _calculate_similarities_for_article_and_store_in_db(self, article: dict, corpus_lda_model, all_sims,
                                                            calculated_sims, content, lda_model_final, content_type):
        cleaned_article = sp.clean_text_for_nlp(article["body"])
        recommendations, mpt = self._find_lda_recommendations(cleaned_article, corpus_lda_model, all_sims,
                                                              calculated_sims, content, lda_model_final, "article")
        if recommendations:
            return db.save_recommendation_scores(article["_id"], content_type, recommendations, mpt)

    def _calculate_similarities_for_book_and_store_in_db(self, oid, cleaned_text: str, corpus_lda_model, all_sims,
                                                         calculated_sims, content, lda_model_final, content_type):
        recommendations, mpt = self._find_lda_recommendations(cleaned_text, corpus_lda_model, all_sims, calculated_sims,
                                                              content, lda_model_final, "book")
        if recommendations:
            return db.save_recommendation_scores(oid, content_type, recommendations, mpt)

    def _calculate_similarities_for_podcast_and_store_in_db(self, oid: ObjectId, episode_index: int,
                                                            corpus_lda_model,
                                                            all_sims, calculated_sims, content,
                                                            lda_model_final,
                                                            content_type):
        cleaned_podcast = db.get_cleaned_podcast_by_original_id(oid)
        cleaned_text = cleaned_podcast["cleanedEpisodes"][episode_index]["cleanedText"]
        recommendations, mpt = self._find_lda_recommendations(cleaned_text, corpus_lda_model, all_sims, calculated_sims,
                                                              content, lda_model_final, "podcast", episode_index)
        if recommendations:
            return db.save_recommendation_scores(oid, content_type, recommendations[:1000], mpt, episode_index)

    def _retrieve_content(self, content_type: str):
        content = []
        if content_type == "article":
            articles = db.get_all_articles()
            for article in articles:
                cleaned_body = sp.clean_text_for_nlp(article["body"])
                content.append({"raw": article["body"], "cleaned": cleaned_body, "oid": article["_id"]})
        elif content_type == "book":
            books = db.get_all_books()
            cleaned_books = db.get_all_cleaned_books()
            for book, c_book in zip(books, cleaned_books):
                raw_text = ""  # " ".join(book["text"])
                cleaned_text = " ".join(c_book["text"])
                content.append({"raw": raw_text, "cleaned": cleaned_text, "oid": book["_id"]})
        elif content_type == "podcast":
            podcasts = db.get_all_podcasts()
            podcast_counter = 0
            for podcast in podcasts:
                c_podcast = db.get_cleaned_podcast_by_original_id(podcast["_id"])
                if len(podcast["entries"]) == len(c_podcast["cleanedEpisodes"]):
                    i = 0
                    for episode, c_episode in zip(podcast["entries"], c_podcast["cleanedEpisodes"]):
                        content.append(
                            {"raw": "", "cleaned": c_episode["cleanedText"], "oid": podcast["_id"], "index": i})
                        i += 1
                else:
                    print(f"inconsistency detected for oid: {podcast['_id']} with len {len(podcast['entries'])} and "
                          f"{len(c_podcast['cleanedEpisodes'])}, podcast num {podcast_counter}")
                    # self._recalculate_cleaned_episodes(podcast, c_podcast)
                podcast_counter += 1

        return content

    def _process_books(self, book) -> dict:
        body = ""
        cleaned_body = ""
        for text in book["text"]:
            body += text + " \n"
            cleaned_body += sp.clean_text_for_nlp(text) + " "
        return {"raw": body, "cleaned": cleaned_body, "oid": book["_id"]}

    def _extract_tokens(self, content: list):
        cleaned_bodies = [item["cleaned"] for item in content]
        text_tokens = [[text for text in cleaned_bodies.split()] for cleaned_bodies in cleaned_bodies]
        # bigrams_for_documents = sp.bigram_extractor(cleaned_bodies)
        if "focus online" in text_tokens:
            text_tokens.remove("focus online")
        return text_tokens

    def _jaccard_similarity(self, query, document):
        intersection = set(query).intersection(set(document))
        union = set(query).union(set(document))
        return float(len(intersection)) / float(len(union))

    def visualize_model(self, topic_num, content_type: str):
        project_folder = os.getcwd()
        lda_model = models.LdaModel.load(project_folder + '/models/ldamodels_bow_' + content_type + '_'
                                         + str(topic_num) + '.lda')
        content = self._retrieve_content(content_type)
        text_tokens = self._extract_tokens(content)
        dictionary = corpora.Dictionary(text_tokens)
        dictionary.filter_extremes(no_below=NO_BELOW, no_above=NO_ABOVE)
        bow_corpus = [dictionary.doc2bow(text) for text in text_tokens]
        pyLDAvis.show(pyLDAvis.gensim.prepare(lda_model, bow_corpus, dictionary))

    def get_statistics(self, participant_id, oid, content_type, episode_index=0) -> dict:
        if type(episode_index) == str:
            try:
                episode_index = int(episode_index)
            except Exception as e:
                logging.error(e)
                episode_index = 0
        statistics = {}
        if db.user_exists(participant_id):
            if content_type == "article":
                article = db.get_item(oid, content_type)
                if article is not None:
                    statistics["title"] = article["title"]
                else:
                    logging.warning(f"could not find article {oid}")
                    return {}  # ToDo: return a placeholder message
                similar_articles = db.get_similar_items(oid, content_type, 10)
                statistics["similarItems"] = similar_articles
                keywords = db.get_item_keywords(oid, content_type)
                statistics["keywords"] = keywords
                readability_index = sp.calculate_readability_index(article["body"])
                statistics["readabilityIndex"] = readability_index
                difficulty = sp.get_difficulty_from_readability_index(readability_index)
                statistics["difficulty"] = difficulty
            elif content_type == "book":
                book = db.get_item(oid, content_type)
                if book is not None:
                    statistics["title"] = book["title"]
                else:
                    logging.warning(f"could not find book {oid}")
                    return {}  # ToDo: return a placeholder message
                similar_books = db.get_similar_items(oid, content_type, 10)
                statistics["similarItems"] = similar_books
                keywords = db.get_item_keywords(oid, content_type)
                statistics["keywords"] = keywords
                if len(book["text"]) > 0:
                    readability_index = sp.calculate_readability_index(" ".join(book["text"]))
                    statistics["readabilityIndex"] = readability_index
                    difficulty = sp.get_difficulty_from_readability_index(readability_index)
                    statistics["difficulty"] = difficulty
            elif content_type == "podcast":
                podcast = db.get_item(oid, content_type)
                if type(episode_index) is str:
                    try:
                        episode_index = int(episode_index)
                    except Exception as e:
                        logging.error(f"could not cast episode index: {e}")
                        return
                if podcast is not None:
                    statistics["title"] = podcast["entries"][episode_index]["title"]
                else:
                    logging.warning(f"could not find podcast {oid}")
                    return {}  # ToDo: return a placeholder message
                similar_episodes = db.get_similar_items(oid, content_type, 10, episode_index)
                statistics["similarItems"] = similar_episodes
                keywords = db.get_item_keywords(oid, content_type)
                statistics["keywords"] = keywords
                if podcast["description"] is not None:
                    readability_index = sp.calculate_readability_index(podcast["description"])
                    statistics["readabilityIndex"] = readability_index
                    difficulty = sp.get_difficulty_from_readability_index(readability_index)
                    statistics["difficulty"] = difficulty
        return statistics

    def create_cleaned_books(self):
        books = db.get_all_books()
        index = 1
        for book in books:
            if db.book_in_cleaned_books(book["_id"]):
                index += 1
                continue
            else:
                print("\r", end='')
                print(f"Book number {index} processing", end='')
                index += 1
            cleaned_text = []
            for page in book["text"]:
                cleaned_text.append(sp.clean_text_for_nlp(page))
            db.save_cleaned_book(
                {
                    "originalId": book["_id"],
                    "text": cleaned_text
                }
            )

    def create_cleaned_podcasts(self):
        podcasts = db.get_all_podcasts()
        index = 1
        podcasts_length = len(podcasts)
        for podcast in podcasts:
            if db.podcast_in_cleaned_podcasts(podcast["_id"]):
                index += 1
                continue
            else:
                print("\r", end='')
                print(f"Podcast number {index} of {podcasts_length} processing", end='')
                index += 1
            cleaned_episodes = []
            for episode in podcast["entries"]:
                cleaned_title = ""
                cleaned_description = ""
                cleaned_summary = ""
                cleaned_keywords = ""
                if episode["title"] is not None:
                    cleaned_title = sp.clean_text_for_nlp(episode["title"])
                if episode["description"] is not None:
                    cleaned_description = sp.clean_text_for_nlp(episode["description"])
                if episode["summary"] is not None:
                    cleaned_summary = sp.clean_text_for_nlp(episode["summary"])
                if episode["keywords"] is not None:
                    cleaned_keywords = sp.clean_text_for_nlp(episode["keywords"])
                cleaned_text = sp.merge_podcast_text(cleaned_title, cleaned_description, cleaned_summary,
                                                     cleaned_keywords)
                cleaned_episodes.append({"cleanedText": cleaned_text})
            db.save_cleaned_podcast(
                {
                    "originalId": podcast["_id"],
                    "cleanedEpisodes": cleaned_episodes
                }
            )
    def _recalculate_cleaned_episodes(self, podcast, c_podcast):
        new_cleaned_episodes = []
        new_cleaned_podcast = copy.deepcopy(c_podcast)
        i = 0
        for episode in podcast["entries"]:
            print(f"\rcleaned {i} of {len(podcast['entries'])}  ", end="")
            cleaned_text = sp.get_cleaned_text_for_episode(episode)
            new_cleaned_episodes.append({"cleanedText": cleaned_text})
            i += 1
        new_cleaned_podcast["cleanedEpisodes"] = new_cleaned_episodes
        db.replace_cleaned_podcast(c_podcast, new_cleaned_podcast)

    def __init__(self):
        pass


if __name__ == "__main__":
    rc = Recommender()
