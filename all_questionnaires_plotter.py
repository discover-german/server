import os
import matplotlib.pyplot as plt
from openpyxl import load_workbook

def plot_all_questionnaires():
    wb_path = ""
    wb = load_workbook(wb_path)
    if not os.path.isdir("qfigs"):
        os.makedirs("qfigs")

    content_ratings_n = 0

    content_1_ratings_B1 = []
    content_2_ratings_B1 = []
    content_3_ratings_B1 = []

    content_1_ratings_B2 = []
    content_2_ratings_B2 = []
    content_3_ratings_B2 = []

    content_1_ratings_C1 = []
    content_2_ratings_C1 = []
    content_3_ratings_C1 = []

    content_titles = []

    for sheet, sheet_name in zip(wb.worksheets, wb.sheetnames):
        language_levels = []
        for col in sheet.columns:
            if col[1].value is None:
                break
            values = [x.value for x in col[2:22]]
            if None in values:
                values = list(filter(None, values))
            if "Language Level" in col[1].value:
                language_levels = values
            if "Inhaltsbewertung" in col[1].value:
                content_ratings_n = len(values)
                if not language_levels:
                    print("No language levels in spreadsheet!")
                    language_levels = [""] * len(values)
                for value, ll in zip(values, language_levels):
                    if value != -1:
                        if "The proposed content matched my personal interests" in col[1].value:
                            if "B1" in ll:
                                content_1_ratings_B1.append(value - 1)
                            elif "B2" in ll:
                                content_1_ratings_B2.append(value - 1)
                            elif "C1" in ll:
                                content_1_ratings_C1.append(value - 1)
                        elif "I felt that the proposed content was personalised for me" in col[1].value:
                            if "B1" in ll:
                                content_2_ratings_B1.append(value - 1)
                            elif "B2" in ll:
                                content_2_ratings_B2.append(value - 1)
                            elif "C1" in ll:
                                content_2_ratings_C1.append(value - 1)
                        elif "The difficulty of the proposed content matched my language level" in col[1].value:
                            if "B1" in ll:
                                content_3_ratings_B1.append(value - 1)
                            elif "B2" in ll:
                                content_3_ratings_B2.append(value - 1)
                            elif "C1" in ll:
                                content_3_ratings_C1.append(value - 1)
                content_titles.append(col[1].value.replace("Inhaltsbewertung: ", ""))

    content_ratings_B1 = [content_1_ratings_B1, content_2_ratings_B1, content_3_ratings_B1]
    content_ratings_B2 = [content_1_ratings_B2, content_2_ratings_B2, content_3_ratings_B2]
    content_ratings_C1 = [content_1_ratings_C1, content_2_ratings_C1, content_3_ratings_C1]
    pairs = [content_ratings_B1, content_ratings_B2, content_ratings_C1]
    fig = plt.figure(figsize=[20, 20])
    fig.suptitle(f"Content evaluation, n={content_ratings_n}", fontsize=18)
    subfigs = fig.subfigures(nrows=3, ncols=1)
    lls = ["B1", "B2", "C1"]
    for row, subfig in enumerate(subfigs):
        subfig.suptitle(f"Language Level: {lls[row]}, n={len(pairs[row][0])}")
        axs = subfig.subplots(nrows=1, ncols=3)
        for col, ax in enumerate(axs):
            ratings = pairs[row][col]
            ax.boxplot(ratings)
            # plt.xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
            ax.set_ylim([0, 4.1])
            # plt.axhline(2, color="r")
            ax.set_title(f"{content_titles[col]}")
    # plt.savefig(f"qfigs/feature_eval_{sheet_name}.svg")
    plt.savefig("qfigs/content_eval_language_levels_all_data.svg")

    plt.show()

if __name__ == "__main__":
    plot_all_questionnaires()