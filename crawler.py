import calendar
import re
import os
import logging
import time

import requests
import chromedriver_autoinstaller
from datetime import datetime
from bs4 import BeautifulSoup
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from database import Database
from string_processing import StringProcessing
from scripts.german_months import german_months
db = Database()
sp = StringProcessing()
if not os.path.isdir("logs"):
    os.makedirs("logs")
logging.basicConfig(filename="logs/crawler", level=logging.DEBUG)

chromedriver_autoinstaller.install()
options = webdriver.ChromeOptions()
options.add_argument("--headless")

SPIEGEL_HEADLINES = "https://www.spiegel.de/schlagzeilen/"
FOCUS_HEADLINES = "https://www.focus.de/schlagzeilen/"
KRUSCHEL_NEWS = "https://kruschel-kinder.de/nachrichten/index.htm"
KRUSCHEL_BASE = "https://kruschel-kinder.de"
LOGO_NEWS = "https://www.zdf.de/kinder/logo"
ZDF_BASE = "https://www.zdf.de"
N_TV_HEADLINES = "https://www.n-tv.de/home/Das_Neueste/"
NACHRICHTENLEICHT_HEADLINES = ["https://www.nachrichtenleicht.de/nachrichtenleicht-nachrichten-100.html",
                               "https://www.nachrichtenleicht.de/nachrichtenleicht-kultur-index-100.html",
                               "https://www.nachrichtenleicht.de/nachrichtenleicht-vermischtes-100.html",
                               "https://www.nachrichtenleicht.de/nachrichtenleicht-sport-100.html"]
PM_HEADLINES = "https://www.pm-wissen.com/artikel/p/highlights/"
PM_BASE = "https://www.pm-wissen.com"
FITFORFUN_HEADLINES = "https://www.fitforfun.de/news"
FITFORFUN_BASE = "https://www.fitforfun.de"
GQ_HEADLINES = "https://www.gq-magazin.de/search"
GQ_BASE = "https://www.gq-magazin.de"
GOFEMININ_HEADLINES = "https://www.gofeminin.de/aktuelles/aktuelles-tp123303.html"
GOFEMININ_BASE = "https://www.gofeminin.de"
GUTENBERG_ALL_BOOKS = "https://www.projekt-gutenberg.org/info/texte/allworka.html"
GUTENBERG_BASE = "https://www.projekt-gutenberg.org/"
LISTENNOTES_BASE = "https://listen-api.listennotes.com/api/v2/"

def aggregate_all_articles() -> dict:
    articles = list()

    spiegel_headline_links = aggregate_spiegel_headlines()
    articles.extend(retrieve_spiegel_articles(spiegel_headline_links))

    focus_headlines = aggregate_focus_headlines()
    articles.extend(retrieve_focus_articles(focus_headlines))

    kruschel_headlines = aggregate_kruschel_headlines()
    articles.extend(retrieve_kruschel_articles(kruschel_headlines))

    logo_headlines = aggregate_logo_headlines()
    articles.extend(retrieve_logo_articles(logo_headlines))

    nl_headline_links = aggregate_nachrichtenleicht_headlines()
    articles.extend(retrieve_nachrichtenleicht_articles(nl_headline_links))

    pm_headline_links = aggregate_pm_headlines()
    articles.extend(retrieve_pm_articles(pm_headline_links))

    fitforfun_headline_links = aggregate_fitforfun_headlines()
    articles.extend(retrieve_fitforfun_articles(fitforfun_headline_links))

    gq_headline_links = aggregate_gq_headlines()
    articles.extend(retrieve_gq_articles(gq_headline_links))

    gofeminin_headline_links = aggregate_gofeminin_headlines()
    articles.extend(retrieve_gofeminin_articles(gofeminin_headline_links))

    save_articles(articles)
    return {'status': 200}

def aggregate_spiegel_headlines() -> list:
    headlines = []
    response = requests.get(SPIEGEL_HEADLINES)
    if response.status_code == 200:
        headlines_soup = BeautifulSoup(response.content, "lxml")
        headline_tags = headlines_soup.find_all("article")
        for tag in headline_tags:
            h2_tag = tag.find("h2")
            if h2_tag is not None and h2_tag.find("svg") is None:
                headline_text = tag.text
                matches = re.search(r"(\d{1,2}. [A-z]+, )*\d{1,2}.\d{1,2} Uhr", headline_text)
                if matches is not None:
                    first_match = matches.group(0)
                    if (_spiegel_article_too_old(first_match)):
                        continue
                    a_tag = tag.find("a")
                    link = a_tag["href"]
                    headlines.append(link)
    return headlines

def aggregate_focus_headlines() -> list:
    headlines = []
    response = requests.get(FOCUS_HEADLINES)
    if response.status_code == 200:
        headlines_soup = BeautifulSoup(response.content, "lxml")
        news_div_tag = headlines_soup.select_one('div[class^="news"]')
        if news_div_tag is not None:
            headline_tags = news_div_tag.find_all("li")
            for tag in headline_tags:
                a_tag = tag.find("a")
                link = a_tag["href"]
                headlines.append(link)
    return headlines

def aggregate_kruschel_headlines() -> list:
    headlines = []
    response = requests.get(KRUSCHEL_NEWS)
    if response.status_code == 200:
        headlines_soup = BeautifulSoup(response.content, "lxml")
        content_tag = headlines_soup.find("div", id="content")
        content_tag.find("div", id="pager").extract()
        if content_tag is not None:
            columns = content_tag.select('div[class*="col"]', recursive=False)
            for col in columns:
                a_tag = col.find("a")
                if a_tag is not None:
                    link = a_tag["href"]
                    headlines.append(KRUSCHEL_BASE + link)
    return headlines

def aggregate_logo_headlines() -> list:
    headlines = []
    response = requests.get(LOGO_NEWS)
    if response.status_code == 200:
        headlines_soup = BeautifulSoup(response.content, "lxml")
        articles = headlines_soup.find_all("article")
        for article in articles:
            h3_tag = article.find("h3")
            if h3_tag is not None:
                a_tag = h3_tag.find("a")
                if a_tag is not None:
                    link = a_tag["href"]
                    headlines.append(ZDF_BASE + link)
    return headlines

def aggregate_nachrichtenleicht_headlines() -> list:
    headlines = []
    for headline_link in NACHRICHTENLEICHT_HEADLINES:
        response = requests.get(headline_link)
        if response.status_code < 400:
            page_soup = BeautifulSoup(response.content, "lxml")
            articles = page_soup.find_all("article")
            for article in articles:
                a_tag = article.find("a")
                if a_tag is not None:
                    link = a_tag["href"]
                    headlines.append(link)
    return headlines

def aggregate_pm_headlines() -> list:
    headlines = []
    driver = webdriver.Chrome(options=options)
    driver.get(PM_HEADLINES)
    time.sleep(3)
    try:
        driver.find_element(By.CSS_SELECTOR, 'button[id*="onetrust-accept-btn-handler"]').click()
    except Exception as e:
        print(e)

    headlines.extend(extract_carousel_items(driver.page_source))

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight*0.25);")
    headlines.extend(extract_carousel_items(driver.page_source))

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight*0.5);")
    headlines.extend(extract_carousel_items(driver.page_source))

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight*0.75);")
    headlines.extend(extract_carousel_items(driver.page_source))

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    headlines.extend(extract_carousel_items(driver.page_source))
    driver.close()
    return list(set(headlines))

def extract_carousel_items(page_source: str) -> list:
    headlines = []
    time.sleep(1)
    headlines_soup = BeautifulSoup(page_source, "lxml")
    articles = headlines_soup.select('li[class*="react-multi-carousel-item"]')
    for article in articles:
        link_tag = article.find('a')
        if link_tag:
            headlines.append(PM_BASE + link_tag["href"])
    return headlines

def aggregate_fitforfun_headlines() -> list:
    headlines = []
    response = requests.get(FITFORFUN_HEADLINES)
    if response.status_code < 400:
        headlines_soup = BeautifulSoup(response.content, "lxml")
        articles = headlines_soup.select('a[class^="teaser"]')
        if articles is not None:
            for article in articles:
                link = article["href"]
                headlines.append(FITFORFUN_BASE + link)
    else:
        logging.error("fitforfun: response code error")
    return headlines

def aggregate_gq_headlines() -> list:
    headlines = []
    driver = webdriver.Chrome(options=options)
    driver.get(GQ_HEADLINES)
    try:
        driver.find_element(By.CSS_SELECTOR, 'button[id*="onetrust-accept-btn-handler"]').click()
    except Exception as e:
        print(e)
    time.sleep(2)
    try:
        driver.find_element(By.CSS_SELECTOR, 'button[class*="SummaryCollectionGridButton-ihXSrJ"]').click()
    except Exception as e:
        print(e)
    time.sleep(2)
    try:
        driver.find_element(By.CSS_SELECTOR, 'button[class*="SummaryCollectionGridButton-ihXSrJ"]').click()
    except Exception as e:
        print(e)
    time.sleep(2)
    try:
        driver.find_element(By.CSS_SELECTOR, 'button[class*="SummaryCollectionGridButton-ihXSrJ"]').click()
    except Exception as e:
        print(e)
    time.sleep(2)
    page_soup = BeautifulSoup(driver.page_source, "lxml")
    article_links = page_soup.select('a[class^="SummaryItemImageLink-gFtgmn"]')
    for article_link in article_links:
        headlines.append(GQ_BASE + article_link["href"])
    driver.close()
    return headlines

def aggregate_gofeminin_headlines() -> list:
    headlines = []
    response = requests.get(GOFEMININ_HEADLINES)
    if response.status_code < 400:
        headline_soup = BeautifulSoup(response.content, "lxml")
        article_icons = headline_soup.select('div[class*="relatedcontent-item article"]')
        if article_icons:
            for icon in article_icons:
                article_link = icon.find("a")
                if article_link:
                    headlines.append(GOFEMININ_BASE + article_link["href"])
    return headlines

def retrieve_spiegel_articles(article_links: list):
    articles = []
    for article_link in article_links:
        logging.info(f"retrieving: {article_link}")
        response = requests.get(article_link)
        if response.status_code == 200:
            article_soup = BeautifulSoup(response.content, "lxml")
            lang = article_soup.html["lang"]
            if lang == "en":
                continue
            title_tag = article_soup.find('title')
            title = ""
            if title_tag is not None:
                title = title_tag.text
            date_tag = article_soup.select_one('meta[name^="date"]')
            date = ""
            if date_tag is not None:
                date = date_tag['content']
            picture_link = ""
            picture_tag = article_soup.select_one('meta[property^="og:image"]')
            if picture_tag is not None:
                picture_link = picture_tag["content"]
            main_tag = article_soup.find("main")
            [x.extract() for x in main_tag.findAll('footer')]
            [x.extract() for x in main_tag.findAll('figure')]
            [x.extract() for x in main_tag.findAll('time')]
            [x.extract() for x in main_tag.select('div[class^="sticky"]')]
            [x.extract() for x in main_tag.select('section[data-outbrain^="FurtherReads"]')]
            [x.extract() for x in main_tag.select('section[class^="clear-both"]')]
            [x.extract() for x in main_tag.select('section[data-area^="contentbox"]')]
            if main_tag:
                text = main_tag.text
                text = re.sub("\t", " ", text)
                text = re.sub(" +", " ", text)
                text = re.sub(r"\n+", "\n", text)
                text = re.sub(r"\n +", "\n", text)
                text = text.strip()
                readability_index = sp.calculate_readability_index(text)
                difficulty = sp.get_difficulty_from_readability_index(readability_index)
                articles.append({'author': 'DER SPIEGEL', "source": "SPIEGEL", 'date': date, 'link': article_link,
                                 'title': title, 'image_link': picture_link, 'body': text, 'added': time.time(),
                                 'difficulty': difficulty})
        sleep(0.5)
    return articles

def retrieve_focus_articles(article_links: list) -> list:
    articles = []
    for article_link in article_links:
        logging.info(f"retrieving: {article_link}")
        response = requests.get(article_link)
        if response.status_code == 200:
            article_soup = BeautifulSoup(response.content, "lxml")
            title_tag = article_soup.find('title')
            title = ""
            if title_tag is not None:
                title = title_tag.text
            date_tag = article_soup.select_one('meta[name^="date"]')
            date = ""
            if date_tag is not None:
                date = date_tag['content']
            picture_link = ""
            picture_tag = article_soup.select_one('meta[property^="og:image"]')
            if picture_tag is not None:
                picture_link = picture_tag["content"]
            text = ""
            article_head = article_soup.select_one('div[class^="articleIdentH1"]')
            if article_head is not None:
                text += article_head.text
            else:
                logging.info("Skipping link since it is not a regular article (head missing)")
                continue  # skip since this is not a regular article
            lead_in = article_soup.select_one('div[class^="leadIn"]')
            if lead_in is None:
                continue
            text += lead_in.text
            text_blocks = article_soup.select('div[class^="textBlock"]')
            for block in text_blocks:
                text += block.text
            text = re.sub("\t", " ", text)
            text = re.sub(" +", " ", text)
            text = re.sub(r"\n+", "\n", text)
            text = re.sub(r"\n +", "\n", text)
            text = text.strip()
            readability_index = sp.calculate_readability_index(text)
            difficulty = sp.get_difficulty_from_readability_index(readability_index)
            articles.append({'author': 'FOCUS Online', "source": "FOCUS",
                             'date': date, 'link': article_link, 'title': title,
                             'image_link': picture_link, 'body': text, 'added': time.time(),
                             'difficulty': difficulty})
        sleep(0.5)
    return articles

def retrieve_kruschel_articles(article_links: list) -> list:
    articles = []
    for article_link in article_links:
        logging.info(f"retrieving: {article_link}")
        response = requests.get(article_link)
        if response.status_code == 200:
            article_soup = BeautifulSoup(response.content, "lxml")
            title_tag = article_soup.find('title')
            title = ""
            if title_tag is not None:
                title = title_tag.text
            date_tag = article_soup.select('meta[name^="date"]')
            date = ""
            if date_tag is not None:
                date = date_tag[0]['content']
            picture_link = ""
            picture_tag = article_soup.select_one('meta[property^="og:image"]')
            if picture_tag is not None:
                picture_link = picture_tag["content"]
            else:
                picture_box = article_soup.select_one('div[class^="image"]')
                if picture_box:
                    picture_tag = picture_box.find("img")
                    if picture_tag and "src" in picture_tag:
                        picture_link = KRUSCHEL_BASE + picture_tag["src"]
            content = article_soup.select_one('div[class^="text"]')
            author_tag = content.select_one('p[class^="author"]')
            if author_tag is not None:
                author_tag.extract()
            media_tag = content.select_one('div[content^="media"]')
            if media_tag is not None:
                media_tag.extract()
            small_tag = content.find("small")
            if small_tag is not None:
                small_tag.extract()
            tools_tag = content.select_one('ul[class^="articletools"]')
            if tools_tag is not None:
                tools_tag.extract()
            send_article_tag = content.find("div", id="send-article")
            if send_article_tag is not None:
                send_article_tag.extract()
            text = content.text
            text = re.sub("\t", " ", text)
            text = re.sub(" +", " ", text)
            text = re.sub(r"\n+", "\n", text)
            text = re.sub(r"\n +", "\n", text)
            text = text.strip()
            readability_index = sp.calculate_readability_index(text)
            difficulty = sp.get_difficulty_from_readability_index(readability_index)
            articles.append({'author': 'Kruschel.de', "source": "KRUSCHEL",
                             'date': date, 'link': article_link, 'title': title,
                             'image_link': picture_link, 'body': text, 'added': time.time(),
                             'difficulty': difficulty})
        sleep(0.5)
    return articles

def retrieve_logo_articles(article_links: list) -> list:
    articles = []
    for article_link in article_links:
        logging.info(f"retrieving: {article_link}")
        try:
            response = requests.get(article_link, timeout=10)
            if response.status_code == 200:
                article_soup = BeautifulSoup(response.content, "lxml")
                footer_tag = article_soup.find("footer")
                if footer_tag is not None:
                    footer_tag.extract()
                main_tag = article_soup.find("main")
                if main_tag is not None:
                    b_post_header = main_tag.select_one('div[class^="b-post-header"]')
                    b_post_content = main_tag.select_one('div[class^="b-post-content"]')
                    if b_post_header is not None and b_post_content is not None:
                        title_tag = article_soup.find('title')
                        title = ""
                        if title_tag is not None:
                            title = title_tag.text.strip()
                        picture_link = ""
                        picture_tag = article_soup.select_one('meta[property^="og:image"]')
                        if picture_tag is not None:
                            picture_link = picture_tag["content"]
                        date_tag = article_soup.select_one('meta[name^="zdf:publicationDate"]')
                        date = ""
                        if date_tag is not None:
                            date = date_tag['content']
                        time_tag = b_post_header.select_one('dl[class^="b-editorial-date"]')
                        if time_tag is not None:
                            time_tag.extract()
                        else:
                            logging.info(f"Skipped article: {article_link}")
                            continue
                        brand_tag = b_post_content.select_one('div[class^="brand-information-container"]')
                        if brand_tag is not None:
                            brand_tag.extract()
                        [x.extract() for x in b_post_header.findAll('figure')]
                        [x.extract() for x in b_post_content.findAll('figure')]
                        [x.extract() for x in b_post_content.select('article[class*="b-content-teaser-item"]')]
                        [x.extract() for x in b_post_content.select('section[class*="b-comments"]')]
                        [x.extract() for x in b_post_content.select('div[class^="play-btn-text"]')]
                        [x.extract() for x in b_post_content.select('div[class*="teaser-box"]')]
                        [x.extract() for x in b_post_content.select('div[class*="load-more-container"]')]
                        [x.extract() for x in b_post_content.select('article[class*="b-cluster"]')]
                        [x.extract() for x in b_post_content.select('article[class*="b-content-teaser-item"]')]
                        [x.extract() for x in b_post_content.select('article[class*="b-cluster-teaser"]')]
                        [x.extract() for x in b_post_content.select('section[class*="b-content-teaser-list"]')]
                        [x.extract() for x in b_post_content.select('span[class^="source"]')]
                        text = ""
                        # text += b_post_header.text
                        for container_tag in b_post_content.select('div[class*="grid-container grid-x"]'):
                            if "diesen text" not in container_tag.text.lower() and \
                                    "geschrieben" not in container_tag.text.lower():
                                text += container_tag.text
                        text = re.sub("\t", " ", text)
                        text = re.sub(" +", " ", text)
                        text = re.sub(r"\n+", "\n", text)
                        text = re.sub(r"\n +", "\n", text)
                        text = text.strip()
                        readability_index = sp.calculate_readability_index(text)
                        difficulty = sp.get_difficulty_from_readability_index(readability_index)
                        if len(re.sub(r"[^a-zÀ-ſ]", "", text)) < 150:
                            logging.info(f"Skipped article: {article_link}")
                            continue
                        articles.append({'author': 'logo!', "source": "LOGO",
                                         'date': date, 'link': article_link, 'title': title,
                                         'image_link': picture_link, 'body': text, 'added': time.time(),
                                         'difficulty': difficulty})
            sleep(0.5)
        except Exception as e:
            logging.error(e)
            print(e)
    return articles

def retrieve_nachrichtenleicht_articles(article_links: list) -> list:
    articles = []
    for article_link in article_links:
        logging.info(f"retrieving: {article_link}")
        response = requests.get(article_link)
        if response.status_code == 200:
            article_soup = BeautifulSoup(response.content, "lxml")
            title_tag = article_soup.find('title')
            title = ""
            if title_tag is not None:
                title = title_tag.text
            date_tag = article_soup.select_one('p[class^="article-header-publication-details"]')
            date = ""
            if date_tag is not None:
                date = date_tag.text
            picture_link = ""
            picture_tag = article_soup.select_one('meta[property^="og:image"]')
            if picture_tag is not None:
                picture_link = picture_tag["content"]
            text = ""
            #headline_title = article_soup.select_one('span[class^="headline-title"]')
            #if headline_title is not None:
            #    text += headline_title.text + "\n"
            headline_description = article_soup.select_one('p[class^="article-header-description"]')
            if headline_description is not None:
                text += headline_description.text + "\n"
            article_texts = article_soup.select('div[class*="article-details-text"]')
            for article_text in article_texts:
                text += article_text.text
            if len(text) < 200:
                logging.error("text too short, something probably went wrong (nachrichtenleicht)")
                continue
            text = re.sub("\t", " ", text)
            text = re.sub(" +", " ", text)
            text = re.sub(r"\n+", "\n", text)
            text = re.sub(r"\n +", "\n", text)
            text = text.strip()
            readability_index = sp.calculate_readability_index(text)
            difficulty = sp.get_difficulty_from_readability_index(readability_index)
            articles.append({'author': 'nachrichtenleicht', "source": "NACHRICHTENLEICHT",
                             'date': date, 'link': article_link, 'title': title,
                             'image_link': picture_link, 'body': text, 'added': time.time(),
                             'difficulty': difficulty})
        sleep(0.5)
    return articles

def retrieve_pm_articles(article_links: list) -> list:
    articles = []
    for article_link in article_links:
        response = requests.get(article_link)
        logging.info(f"retrieving: {article_link}")
        if response.status_code < 400:
            try:
                article_soup = BeautifulSoup(response.content, "lxml")
                title_tag = article_soup.find("title")
                title = ""
                if title_tag:
                    title = title_tag.text
                date = ""
                date_tag = article_soup.select_one('meta[property^="article:published_time"]')
                if date_tag and "content" in date_tag.attrs:
                    date = date_tag["content"]
                picture_link = ""
                picture_tag = article_soup.select_one('meta[property^="og:image"]')
                if picture_tag and "content" in picture_tag.attrs:
                    picture_link = picture_tag["content"]
                article_body = article_soup.select_one('div[class^="css-evsyos"]')
                text = ""
                headline = article_soup.find("h1")
                if headline:
                    text += headline.text + "\n"
                if article_body:
                    for div in article_body.find_all("div", recursive=False):
                        if div.find("img") is None:
                            text += div.text
                text = re.sub("\t", " ", text)
                text = re.sub(" +", " ", text)
                text = re.sub(r"\n+", "\n", text)
                text = re.sub(r"\n +", "\n", text)
                text = text.strip()
                if len(text) < 100:
                    continue
                readability_index = sp.calculate_readability_index(text)
                difficulty = sp.get_difficulty_from_readability_index(readability_index)
                articles.append({'author': 'P.M.', "source": "PM",
                                 'date': date, 'link': article_link, 'title': title,
                                 'image_link': picture_link, 'body': text, 'added': time.time(),
                                 'difficulty': difficulty})
                sleep(0.5)
            except Exception as e:
                logging.error(f"pm error: {e}")
        else:
            logging.error(f"pm: status code error for {article_link}")
    return articles

def retrieve_fitforfun_articles(article_links: list) -> list:
    articles = []
    for article_link in article_links:
        response = requests.get(article_link)
        if response.status_code < 400:
            try:
                article_soup = BeautifulSoup(response.content, "lxml")
                title_tag = article_soup.find("title")
                title = ""
                if title_tag:
                    title = title_tag.text
                date = ""
                date_tags = article_soup.select('span[class^="date"]')
                for tag in date_tags:
                    if tag and "content" in tag.attrs:
                        date = tag["content"]
                        break
                picture_link = ""
                picture_tag = article_soup.select_one('meta[property^="og:image"]')
                if picture_tag and "content" in picture_tag.attrs:
                    picture_link = picture_tag["content"]
                text = ""
                header_tag = article_soup.select_one('header[class^="intro"]')
                if header_tag:
                    header_title_tag = header_tag.find("h1")
                    if header_title_tag:
                        header_title = header_title_tag.text
                        text += f"{header_title} \n"
                    small_heading_tag = header_tag.select_one('p[class^="heading"]')
                    if small_heading_tag:
                        small_heading = small_heading_tag.text
                        text += f"{small_heading} \n"
                article_column_tag = article_soup.select_one('div[class^="article-column"]')
                if article_column_tag:
                    text_boxes = article_column_tag.select('div[class*="clearfix whiteblock"]')
                    if text_boxes:
                        for box in text_boxes:
                            text += f"{box.text} \n"
                text = re.sub("\t", " ", text)
                text = re.sub(" +", " ", text)
                text = re.sub(r"\n+", "\n", text)
                text = re.sub(r"\n +", "\n", text)
                text = text.strip()
                if len(text) < 100:
                    continue
                readability_index = sp.calculate_readability_index(text)
                difficulty = sp.get_difficulty_from_readability_index(readability_index)
                articles.append({'author': 'FitForFun', "source": "FITFORFUN",
                                 'date': date, 'link': article_link, 'title': title,
                                 'image_link': picture_link, 'body': text, 'added': time.time(),
                                 'difficulty': difficulty})
                sleep(0.5)
            except Exception as e:
                logging.error(f"fitforfun error: {e}")
        else:
            logging.error(f"fitforfun: response error for {article_link}")
    return articles

def retrieve_gq_articles(article_links: list) -> list:
    articles = []
    driver = webdriver.Chrome(options=options)
    for article_link in article_links:
        try:
            driver.get(article_link)
            article_soup = BeautifulSoup(driver.page_source, "lxml")
            title_tag = article_soup.find("title")
            title = ""
            if title_tag:
                title = title_tag.text
            date = ""
            date_tag = article_soup.select_one('meta[property^="article:published_time"]')
            if date_tag and "content" in date_tag.attrs:
                date = date_tag["content"]
            picture_link = ""
            picture_tag = article_soup.select_one('meta[property^="og:image"]')
            if picture_tag and "content" in picture_tag.attrs:
                picture_link = picture_tag["content"]
            text = ""
            content_header_container = article_soup.select_one('div[class*="ContentHeaderContainer"]')
            if content_header_container:
                headline_tag = content_header_container.find("h1")
                if headline_tag:
                    text += f"{headline_tag.text} \n"
                small_header = content_header_container.select_one('div[class*="ContentHeaderDek"]')
                if small_header:
                    text += f"{small_header.text} \n"
            text_containers = article_soup.select('div[class*="inner-container"]')
            for container in text_containers:
                text += f"{container.text} \n"
            text = re.sub("\t", " ", text)
            text = re.sub(" +", " ", text)
            text = re.sub(r"\n+", "\n", text)
            text = re.sub(r"\n +", "\n", text)
            text = text.strip()
            if len(text) < 100:
                continue
            readability_index = sp.calculate_readability_index(text)
            difficulty = sp.get_difficulty_from_readability_index(readability_index)
            articles.append({'author': 'GQ Magazin', "source": "GQ",
                             'date': date, 'link': article_link, 'title': title,
                             'image_link': picture_link, 'body': text, 'added': time.time(),
                             'difficulty': difficulty})
            sleep(0.5)
        except Exception as e:
            logging.error(f"gq error: {e}")
    driver.close()
    return articles

def retrieve_gofeminin_articles(article_links: list) -> list:
    articles = []
    for article_link in article_links:
        response = requests.get(article_link)
        if response.status_code < 400:
            article_soup = BeautifulSoup(response.content, "lxml")
            try:
                title_tag = article_soup.find("title")
                title = ""
                if title_tag:
                    title = title_tag.text
                date = ""
                date_tag = article_soup.select_one('meta[name*="article:published_time"]')
                if date_tag and "content" in date_tag.attrs:
                    date = date_tag["content"]
                picture_link = ""
                picture_tag = article_soup.select_one('meta[property^="og:image"]')
                if picture_tag and "content" in picture_tag.attrs:
                    picture_link = picture_tag["content"]
                text = ""
                header_container = article_soup.select_one('div[class^="af-story-header-content"]')
                if header_container:
                    headline = header_container.find("h1")
                    if headline:
                        text += f"{headline.text} \n"
                text_column = article_soup.select_one('div[class*="af-story-content"]')
                if text_column:
                    text_paragraphs = text_column.find_all('p')
                    for paragraph in text_paragraphs:
                        text += f"{paragraph.text} \n"
                text = re.sub("\t", " ", text)
                text = re.sub(" +", " ", text)
                text = re.sub(r"\n+", "\n", text)
                text = re.sub(r"\n +", "\n", text)
                text = text.strip()
                if len(text) < 100:
                    continue
                readability_index = sp.calculate_readability_index(text)
                difficulty = sp.get_difficulty_from_readability_index(readability_index)
                articles.append({'author': 'gofeminin', "source": "GOFEMININ",
                                 'date': date, 'link': article_link, 'title': title,
                                 'image_link': picture_link, 'body': text, 'added': time.time(),
                                 'difficulty': difficulty})
                sleep(0.5)
            except Exception as e:
                logging.error(f"gofeminin error: {e}")
        else:
            logging.error(f"gofeminin: response error for {article_link}")
    return articles

def save_articles(articles):
    for article in articles:
        db.save_article(article)
    return

def _spiegel_article_too_old(first_match: str) -> bool:
    if len(first_match) > 9:
        month = first_match.split()[1].replace(",", "").lower()
        day = int(first_match.split()[0].replace(".", ""))
        month_as_num = german_months[month]
        currentMonth = datetime.now().month
        currentDay = datetime.now().day
        year = datetime.now().year
        if currentDay == 1 and currentMonth == 1:
            year = year - 1
        if month_as_num == currentMonth and day < currentDay - 1:
            return True
        elif month_as_num < currentMonth and day < calendar.monthrange(year, month_as_num)[1]:
            return True
    return False

def aggregate_gutenberg() -> list:
    books = []
    response = requests.get(GUTENBERG_ALL_BOOKS)
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, "lxml")
        dl_tag = soup.find("dl")
        if dl_tag is not None:
            book_links = dl_tag.find_all("a")
            book_counter = 0
            for link_tag in book_links[10488+298:]:
                link = link_tag["href"]
                link_name = link_tag.text
                relevant_part = "/".join(link.split("/")[2:])
                correct_link = GUTENBERG_BASE + relevant_part
                book = retrieve_book(correct_link, link_name)
                if book is not None:
                    books.append(book)
                    db.save_book(book)
                    book_counter += 1
                    print(f"book number {book_counter} saved to db")
                    sleep(0.5)
    return books

def retrieve_book(book_link: str, link_name: str) -> dict:
    try:
        response = requests.get(book_link)
    except requests.exceptions.MissingSchema as e:
        print(e)
        response = None
    book = {}
    if response is not None and response.status_code == 200:
        author = ""
        title = ""
        pages = []
        first_soup = BeautifulSoup(response.content, "lxml")
        author_selection = first_soup.select('h3[class^="author"]')
        title_selection = first_soup.select('h2[class^="title"]')
        if author_selection is not None and len(author_selection) > 0:
            author = author_selection[0].text
        if title_selection is not None and len(title_selection) > 0:
            title = title_selection[0].text
        else:
            title = link_name
        title = re.sub(r"\t", " ", title)
        title = re.sub(r"\n", " ", title)
        title = re.sub(r" +", " ", title)
        image_tag = first_soup.select_one('img[alt="Titelblatt"]')
        image_link = None
        if image_tag is not None:
            image_link = "/".join(book_link.split("/")[:-1]) + "/" + image_tag["src"]
        page_links = first_soup.find_all("a")
        next_soup = get_next_page_soup(book_link, page_links)
        print(f"Saving book {title} by {author}")
        pagecounter = 0
        while next_soup != None:
            page_text = ""
            big_title = next_soup.find("h2")
            if big_title is not None:
                page_text += "##" + big_title.text + "\n"
            page_title = next_soup.find("h3")
            if page_title is not None:
                page_text += "###" + page_title.text + "\n"
            p_tags = next_soup.find_all("p")
            for p_tag in p_tags:
                page_text += p_tag.text + "\n"
            page_text = re.sub("\t", " ", page_text)
            page_text = re.sub(" +", " ", page_text)
            page_text = re.sub(r"\n+", "\n", page_text)
            page_text = re.sub(r"\n +", "\n", page_text)
            page_text = page_text.strip()
            pages.append(page_text)
            page_links = next_soup.find_all("a")
            next_soup = get_next_page_soup(book_link, page_links)
            sleep(0.25)
            pagecounter += 1
            print(f"saved page {pagecounter}")
        book = {"author": author, "title": title, "link": book_link, "text": pages, "image_link": image_link}
    return book

def get_next_page_soup(base_link: str, links: list) -> BeautifulSoup:
    for link in links:
        if 'weiter >>' in link.text:
            link_href = link["href"]
            correct_link = "/".join(base_link.split("/")[:-1]) + "/" + link_href
            response = requests.get(correct_link)
            if response.status_code == 200:
                return BeautifulSoup(response.content, "lxml")

def save_books(books: list):
    for book in books:
        db.save_book(book)
    return

def aggregate_best_podcasts() -> dict:
    podcasts = {}
    return podcasts

if __name__ == "__main__":
    aggregate_all_articles()
    #aggregate_gutenberg()
