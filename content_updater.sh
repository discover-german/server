#!/bin/bash
cd /home/learnrec/ContentCrawler
source venv/bin/activate
export PYTHONPATH="${PYTHONPATH}:."
python -u content_updater.py
echo "your password here" | sudo systemctl restart mongodb
pm2 stop server_start
pm2 start server_start --exp-backoff-restart-delay=100
