import os
import matplotlib.pyplot as plt
from openpyxl import load_workbook


def plot_split_questionnaire():
    wb_path = ""
    wb = load_workbook(wb_path)
    if not os.path.isdir("qfigs"):
        os.makedirs("qfigs")

    would_use_in_future_1_rand = 0
    would_use_in_future_2_rand = 0
    would_use_in_future_3_rand = 0
    would_use_in_future_1_pers = 0
    would_use_in_future_2_pers = 0
    would_use_in_future_3_pers = 0

    functionality_1_rand = 0
    functionality_2_rand = 0
    functionality_3_rand = 0
    functionality_1_pers = 0
    functionality_2_pers = 0
    functionality_3_pers = 0

    feature_1_ratings_rand = []
    feature_2_ratings_rand = []
    feature_3_ratings_rand = []
    feature_4_ratings_rand = []
    feature_5_ratings_rand = []
    feature_6_ratings_rand = []
    feature_7_ratings_rand = []
    feature_8_ratings_rand = []
    feature_9_ratings_rand = []
    feature_10_ratings_rand = []
    feature_11_ratings_rand = []
    feature_1_ratings_pers = []
    feature_2_ratings_pers = []
    feature_3_ratings_pers = []
    feature_4_ratings_pers = []
    feature_5_ratings_pers = []
    feature_6_ratings_pers = []
    feature_7_ratings_pers = []
    feature_8_ratings_pers = []
    feature_9_ratings_pers = []
    feature_10_ratings_pers = []
    feature_11_ratings_pers = []

    content_1_ratings_rand = []
    content_2_ratings_rand = []
    content_3_ratings_rand = []
    content_1_ratings_pers = []
    content_2_ratings_pers = []
    content_3_ratings_pers = []

    content_1_ratings_rand_B1 = []
    content_2_ratings_rand_B1 = []
    content_3_ratings_rand_B1 = []
    content_1_ratings_pers_B1 = []
    content_2_ratings_pers_B1 = []
    content_3_ratings_pers_B1 = []

    content_1_ratings_rand_B2 = []
    content_2_ratings_rand_B2 = []
    content_3_ratings_rand_B2 = []
    content_1_ratings_pers_B2 = []
    content_2_ratings_pers_B2 = []
    content_3_ratings_pers_B2 = []

    content_1_ratings_rand_C1 = []
    content_2_ratings_rand_C1 = []
    content_3_ratings_rand_C1 = []
    content_1_ratings_pers_C1 = []
    content_2_ratings_pers_C1 = []
    content_3_ratings_pers_C1 = []

    did_not_use_1_rand = 0
    did_not_use_2_rand = 0
    did_not_use_3_rand = 0
    did_not_use_4_rand = 0
    did_not_use_5_rand = 0
    did_not_use_6_rand = 0
    did_not_use_7_rand = 0
    did_not_use_8_rand = 0
    did_not_use_9_rand = 0
    did_not_use_10_rand = 0
    did_not_use_11_rand = 0
    did_not_use_1_pers = 0
    did_not_use_2_pers = 0
    did_not_use_3_pers = 0
    did_not_use_4_pers = 0
    did_not_use_5_pers = 0
    did_not_use_6_pers = 0
    did_not_use_7_pers = 0
    did_not_use_8_pers = 0
    did_not_use_9_pers = 0
    did_not_use_10_pers = 0
    did_not_use_11_pers = 0

    dont_know_1_rand = 0
    dont_know_2_rand = 0
    dont_know_3_rand = 0
    dont_know_1_pers = 0
    dont_know_2_pers = 0
    dont_know_3_pers = 0

    feature_ratings_n = 0
    content_ratings_n = 0

    feature_titles = []
    content_titles = []
    for sheet, sheet_name in zip(wb.worksheets, wb.sheetnames):
        if sheet_name == "All":
            continue
        language_levels = []
        for col in sheet.columns:
            if col[1].value is None:
                break
            values = [x.value for x in col[2:22]]
            if None in values:
                values = list(filter(None, values))
            if "Language Level" in col[1].value:
                language_levels = values
            if "Funktionalität" in col[1].value:
                for value in values:
                    if value == 1:
                        if sheet_name == "Random":
                            functionality_1_rand += 1
                        elif sheet_name == "Personalised":
                            functionality_1_pers += 1
                    elif value == 2:
                        if sheet_name == "Random":
                            functionality_2_rand += 1
                        elif sheet_name == "Personalised":
                            functionality_2_pers += 1
                    elif value == -1:
                        if sheet_name == "Random":
                            functionality_3_rand += 1
                        elif sheet_name == "Personalised":
                            functionality_3_pers += 1
            if "Verwendung" in col[1].value:
                for value in values:
                    if value == 1:
                        if sheet_name == "Random":
                            would_use_in_future_1_rand += 1
                        elif sheet_name == "Personalised":
                            would_use_in_future_1_pers += 1
                    elif value == 2:
                        if sheet_name == "Random":
                            would_use_in_future_2_rand += 1
                        elif sheet_name == "Personalised":
                            would_use_in_future_2_pers += 1
                    elif value == 3:
                        if sheet_name == "Random":
                            would_use_in_future_3_rand += 1
                        elif sheet_name == "Personalised":
                            would_use_in_future_3_pers += 1
            elif "Feature-Bewertung" in col[1].value:
                feature_ratings_n = len(values)
                for value in values:
                    if "Receiving notifications" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_1_rand += 1
                            else:
                                feature_1_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_1_pers += 1
                            else:
                                feature_1_ratings_pers.append(value - 1)
                    elif "Set custom notifications" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_2_rand += 1
                            else:
                                feature_2_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_2_pers += 1
                            else:
                                feature_2_ratings_pers.append(value - 1)
                    elif "Translation and dictionary" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_3_rand += 1
                            else:
                                feature_3_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_3_pers += 1
                            else:
                                feature_3_ratings_pers.append(value - 1)
                    elif "Saving words and phrases" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_4_rand += 1
                            else:
                                feature_4_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_4_pers += 1
                            else:
                                feature_4_ratings_pers.append(value - 1)
                    elif "Flashcard feature" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_5_rand += 1
                            else:
                                feature_5_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_5_pers += 1
                            else:
                                feature_5_ratings_pers.append(value - 1)
                    elif "Removing content from your feed by dismissing it" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_6_rand += 1
                            else:
                                feature_6_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_6_pers += 1
                            else:
                                feature_6_ratings_pers.append(value - 1)
                    elif "Cleaned view of news articles and books" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_7_rand += 1
                            else:
                                feature_7_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_7_pers += 1
                            else:
                                feature_7_ratings_pers.append(value - 1)
                    elif "Translated view of news articles and books" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_8_rand += 1
                            else:
                                feature_8_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_8_pers += 1
                            else:
                                feature_8_ratings_pers.append(value - 1)
                    elif "Saving items as favorites" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_9_rand += 1
                            else:
                                feature_9_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_9_pers += 1
                            else:
                                feature_9_ratings_pers.append(value - 1)
                    elif "Viewing history" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_10_rand += 1
                            else:
                                feature_10_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_10_pers += 1
                            else:
                                feature_10_ratings_pers.append(value - 1)
                    elif "Content filter" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                did_not_use_11_rand += 1
                            else:
                                feature_11_ratings_rand.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                did_not_use_11_pers += 1
                            else:
                                feature_11_ratings_pers.append(value - 1)
                feature_titles.append(col[1].value.replace("Feature-Bewertung: ", ""))
            elif "Inhaltsbewertung" in col[1].value:
                content_ratings_n = len(values)
                if not language_levels:
                    print("No language levels in spreadsheet!")
                    language_levels = [""] * len(values)
                for value, ll in zip(values, language_levels):
                    if "The proposed content matched my personal interests" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                dont_know_1_rand += 1
                            else:
                                content_1_ratings_rand.append(value - 1)
                                if "B1" in ll:
                                    content_1_ratings_rand_B1.append(value - 1)
                                elif "B2" in ll:
                                    content_1_ratings_rand_B2.append(value - 1)
                                elif "C1" in ll:
                                    content_1_ratings_rand_C1.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                dont_know_1_pers += 1
                            else:
                                content_1_ratings_pers.append(value - 1)
                                if "B1" in ll:
                                    content_1_ratings_pers_B1.append(value - 1)
                                elif "B2" in ll:
                                    content_1_ratings_pers_B2.append(value - 1)
                                elif "C1" in ll:
                                    content_1_ratings_pers_C1.append(value - 1)
                    elif "I felt that the proposed content was personalised for me" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                dont_know_2_rand += 1
                            else:
                                content_2_ratings_rand.append(value - 1)
                                if "B1" in ll:
                                    content_2_ratings_rand_B1.append(value - 1)
                                elif "B2" in ll:
                                    content_2_ratings_rand_B2.append(value - 1)
                                elif "C1" in ll:
                                    content_2_ratings_rand_C1.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                dont_know_2_pers += 1
                            else:
                                content_2_ratings_pers.append(value - 1)
                                if "B1" in ll:
                                    content_2_ratings_pers_B1.append(value - 1)
                                elif "B2" in ll:
                                    content_2_ratings_pers_B2.append(value - 1)
                                elif "C1" in ll:
                                    content_2_ratings_pers_C1.append(value - 1)
                    elif "The difficulty of the proposed content matched my language level" in col[1].value:
                        if sheet_name == "Random":
                            if value == -1:
                                dont_know_3_rand += 1
                            else:
                                content_3_ratings_rand.append(value - 1)
                                if "B1" in ll:
                                    content_3_ratings_rand_B1.append(value - 1)
                                elif "B2" in ll:
                                    content_3_ratings_rand_B2.append(value - 1)
                                elif "C1" in ll:
                                    content_3_ratings_rand_C1.append(value - 1)
                        elif sheet_name == "Personalised":
                            if value == -1:
                                dont_know_3_pers += 1
                            else:
                                content_3_ratings_pers.append(value - 1)
                                if "B1" in ll:
                                    content_3_ratings_pers_B1.append(value - 1)
                                elif "B2" in ll:
                                    content_3_ratings_pers_B2.append(value - 1)
                                elif "C1" in ll:
                                    content_3_ratings_pers_C1.append(value - 1)
                content_titles.append(col[1].value.replace("Inhaltsbewertung: ", ""))

    feature_ratings = [(feature_1_ratings_rand, feature_1_ratings_pers),
                       (feature_2_ratings_rand, feature_2_ratings_pers),
                       (feature_3_ratings_rand, feature_3_ratings_pers),
                       (feature_4_ratings_rand, feature_4_ratings_pers),
                       (feature_5_ratings_rand, feature_5_ratings_pers),
                       (feature_6_ratings_rand, feature_6_ratings_pers),
                       (feature_7_ratings_rand, feature_7_ratings_pers),
                       (feature_8_ratings_rand, feature_8_ratings_pers),
                       (feature_9_ratings_rand, feature_9_ratings_pers),
                       (feature_10_ratings_rand, feature_10_ratings_pers),
                       (feature_11_ratings_rand, feature_11_ratings_pers)]
    f, ax = plt.subplots(int(len(feature_ratings) / 3) + 1, 3, figsize=[16, 12])
    for i, pair in enumerate(feature_ratings):
        row = int(i / 3)
        col = i % 3
        ax[row, col].boxplot(pair, labels=["Random", "Personalised"])
        # plt.xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
        ax[row, col].set_ylim([0, 4.1])
        # plt.axhline(2, color="r")
        ax[row, col].set_title(f"{feature_titles[i]}")
    ax[-1, -1].axis('off')
    # plt.savefig(f"qfigs/feature_eval_{sheet_name}.svg")
    plt.subplots_adjust(hspace=0.3)
    f.suptitle(f"Feature evaluation, n={feature_ratings_n}", fontsize=18)
    plt.savefig("qfigs/feature_eval.svg")

    did_not_use_ratings = [(did_not_use_1_rand, did_not_use_1_pers),
                           (did_not_use_2_rand, did_not_use_2_pers),
                           (did_not_use_3_rand, did_not_use_3_pers),
                           (did_not_use_4_rand, did_not_use_4_pers),
                           (did_not_use_5_rand, did_not_use_5_pers),
                           (did_not_use_6_rand, did_not_use_6_pers),
                           (did_not_use_7_rand, did_not_use_7_pers),
                           (did_not_use_8_rand, did_not_use_8_pers),
                           (did_not_use_9_rand, did_not_use_9_pers),
                           (did_not_use_10_rand, did_not_use_10_pers),
                           (did_not_use_11_rand, did_not_use_11_pers)]
    f, ax = plt.subplots(int(len(did_not_use_ratings) / 3) + 1, 3, figsize=[16, 12])
    for i, pair in enumerate(did_not_use_ratings):
        row = int(i / 3)
        col = i % 3
        ax[row, col].bar(x=(0, 1), height=pair)
        ax[row, col].set_xticks((0, 1), ["Random", "Personalised"], rotation="horizontal")
        ax[row, col].set_ylim([0, 7.5])
        ax[row, col].set_yticks(range(0, 8), range(0, 8))
        # plt.axhline(2, color="r")
        ax[row, col].set_title(f"{feature_titles[i]}")
    ax[-1, -1].axis('off')
    # plt.savefig(f"qfigs/feature_eval_{sheet_name}.svg")
    plt.subplots_adjust(hspace=0.3)
    f.suptitle(f'Number of responses for "did not use", n={feature_ratings_n}', fontsize=18)
    plt.savefig("qfigs/did_not_use_eval.svg")

    content_ratings = [(content_1_ratings_rand, content_1_ratings_pers),
                       (content_2_ratings_rand, content_2_ratings_pers),
                       (content_3_ratings_rand, content_3_ratings_pers)]
    f, ax = plt.subplots(1, 3, figsize=[20, 6])
    for i, pair in enumerate(content_ratings):
        ax[i].boxplot(pair, labels=["Random", "Personalised"])
        # plt.xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
        ax[i].set_ylim([0, 4.1])
        # plt.axhline(2, color="r")
        ax[i].set_title(f"{content_titles[i]}")
    # plt.savefig(f"qfigs/feature_eval_{sheet_name}.svg")
    f.suptitle(f"Content evaluation, n={content_ratings_n}", fontsize=18)
    plt.savefig("qfigs/content_eval.svg")

    content_ratings_B1 = [(content_1_ratings_rand_B1, content_1_ratings_pers_B1),
                          (content_2_ratings_rand_B1, content_2_ratings_pers_B1),
                          (content_3_ratings_rand_B1, content_3_ratings_pers_B1)]
    content_ratings_B2 = [(content_1_ratings_rand_B2, content_1_ratings_pers_B2),
                          (content_2_ratings_rand_B2, content_2_ratings_pers_B2),
                          (content_3_ratings_rand_B2, content_3_ratings_pers_B2)]
    content_ratings_C1 = [(content_1_ratings_rand_C1, content_1_ratings_pers_C1),
                          (content_2_ratings_rand_C1, content_2_ratings_pers_C1),
                          (content_3_ratings_rand_C1, content_3_ratings_pers_C1)]
    pairs = [content_ratings_B1, content_ratings_B2, content_ratings_C1]
    fig = plt.figure(figsize=[20, 20])
    fig.suptitle(f"Content evaluation, n={content_ratings_n}", fontsize=18)
    subfigs = fig.subfigures(nrows=3, ncols=1)
    lls = ["B1", "B2", "C1"]
    for row, subfig in enumerate(subfigs):
        subfig.suptitle(f"Language Level: {lls[row]}, n={len(pairs[row][0][0])}")
        axs = subfig.subplots(nrows=1, ncols=3)
        for col, ax in enumerate(axs):
            pair = pairs[row][col]
            ax.boxplot(pair, labels=["Random", "Personalised"])
            # plt.xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
            ax.set_ylim([0, 4.1])
            # plt.axhline(2, color="r")
            ax.set_title(f"{content_titles[col]}")
    # plt.savefig(f"qfigs/feature_eval_{sheet_name}.svg")
    plt.savefig("qfigs/content_eval_language_levels_split_data.svg")

    dont_know_ratings = [(dont_know_1_rand, dont_know_1_pers),
                         (dont_know_2_rand, dont_know_2_pers),
                         (dont_know_2_rand, dont_know_2_pers)]
    f, ax = plt.subplots(1, 3, figsize=[20, 6])
    for i, pair in enumerate(dont_know_ratings):
        ax[i].bar(x=range(0, 2), height=pair)
        ax[i].set_xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
        ax[i].set_title(f"{content_titles[i]}")
    # plt.savefig(f"qfigs/feature_eval_{sheet_name}.svg")
    f.suptitle(f'Number of responses for "don\'t know", n={content_ratings_n}', fontsize=18)
    plt.savefig("qfigs/dont_know_eval.svg")

    functionality_responses = [(functionality_1_rand, functionality_1_pers),
                               (functionality_2_rand, functionality_2_pers),
                               (functionality_3_rand, functionality_3_pers)]

    functionality_titles = ["Yes", "No", "I disabled all notifications."]

    f, ax = plt.subplots(1, 3, figsize=[20, 6])
    for i, pair in enumerate(functionality_responses):
        ax[i].bar(x=range(0, 2), height=pair)
        ax[i].set_xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
        ax[i].set_title(f"{functionality_titles[i]}")
    f.suptitle(f'Did you receive any notifications on your phone (from the app)?", n={content_ratings_n}', fontsize=18)
    plt.savefig("qfigs/notifications_eval.svg")

    would_use_in_future_responses = [(would_use_in_future_1_rand, would_use_in_future_1_pers),
                                     (would_use_in_future_2_rand, would_use_in_future_2_pers),
                                     (would_use_in_future_3_rand, would_use_in_future_3_pers)]

    would_use_in_future_titles = ["Yes", "No", "Maybe"]

    f, ax = plt.subplots(1, 3, figsize=[20, 6])
    for i, pair in enumerate(would_use_in_future_responses):
        ax[i].bar(x=range(0, 2), height=pair)
        ax[i].set_xticks(range(0, 2), ["Random", "Personalised"], rotation="horizontal")
        ax[i].set_title(f"{would_use_in_future_titles[i]}")
    f.suptitle(f'Would you like to continue using the App in the future?", n={content_ratings_n}', fontsize=18)
    plt.savefig("qfigs/would_use_in_future_eval.svg")

    plt.show()


if __name__ == "__main__":
    plot_split_questionnaire()
