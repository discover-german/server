# app.py:
import json
import gzip
import scripts.user_management
from flask import Flask, jsonify, request, make_response, abort
from recommender import Recommender
from string_processing import StringProcessing
from database import Database

app = Flask(__name__)
rc = Recommender()
sp = StringProcessing()
db = Database()

@app.route("/")
def index() -> str:
    # transform a dict into an application/json response
    return jsonify({"message": "It Works"})

@app.route("/recommendations")
def recommendations() -> str:
    participant_id = request.headers.get('participantId')
    blacklist = json.loads(request.args.get('blacklist', default="[]"))
    cutoff = json.loads(request.args.get('cutoff', default='8'))
    difficulty = json.loads(request.args.get('difficulty', default='2'))
    return make_response(gzip.compress(json.dumps(rc.get_recommendations(participant_id, blacklist, cutoff, difficulty))
                                       .encode("utf8")))

@app.route("/register_interest", methods = ['PUT'])
def register_interest() -> str:
    participant_id = request.headers.get('participantId')
    data = request.form
    content_type = data.get('contentType')
    content_id = data.get('oid')
    episode_index = data.get('episodeIndex')
    return jsonify(scripts.user_management.register_interest(participant_id, content_type, content_id, episode_index))

@app.route("/statistics")
def statistics() -> str:
    participant_id = request.headers.get('participantId')
    oid = request.args.get("oid")
    content_type = request.args.get('contentType')
    episode_index = request.args.get('episodeIndex')
    return jsonify(rc.get_statistics(participant_id, oid, content_type, episode_index))

@app.route("/item")
def item() -> str:
    participant_id = request.headers.get('participantId')
    oid = request.args.get("oid", default="")
    content_type = request.args.get('contentType', default="")
    blacklist = json.loads(request.args.get('blacklist', default="[]"))
    difficulty = request.args.get('difficulty', default="2")
    if db.user_exists(participant_id):
        if oid == "" and db.get_personalised(participant_id) and difficulty != "":
            response = rc.get_personalised_item(participant_id, content_type, blacklist, difficulty)
        else:
            response = db.get_parsed_item(oid, content_type)
        return make_response(gzip.compress(json.dumps(response).encode("utf-8")))

@app.route("/add_logs", methods = ['PUT'])
def add_logs() -> str:
    participant_id = request.headers.get('participantId')
    data = request.form
    logs = data.get("logs")
    if db.user_exists(participant_id):
        db.add_logs(participant_id, logs)
        return jsonify({"message": f"Sucessfully added {len(logs)} logs"})
    else:
        abort(400, 'Record not found')

@app.route("/dismiss", methods = ['PUT'])
def dismiss() -> str:
    participant_id = request.headers.get('participantId')
    data = request.form
    oid = data.get("oid")
    content_type = data.get('contentType')
    episode_index = data.get('episodeIndex')
    if db.user_exists(participant_id):
        db.add_dismissed_item(participant_id, oid, content_type, episode_index)
        return jsonify({"message": "Sucessfully dismissed one item}"})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=54500, ssl_context=('../certs/server.crt', '../certs/server.key'))
