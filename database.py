import copy
import json
import re
import time
import asyncio
import aioschedule as schedule

from bson import json_util, ObjectId
from pymongo import MongoClient
from pymongo.errors import DocumentTooLarge

from constants import ARTICLES_HALFLIFE

client = MongoClient()


def singleton(class_):
    instances = {}

    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]

    return getinstance


@singleton
class Database:
    db = client.content

    def __init__(self):
        db_names = self.db.list_collection_names()
        if "similarities" in db_names:
            self.db.similarities.create_index([("originalId", 1), ("contentType", 1), ("episodeIndex", 1)])
        if "articles" in db_names:
            self.db.articles.create_index([("title", 1), ("author", 1)])
        if "podcasts" in db_names:
            self.db.podcasts.create_index([("title", 1), ("link", 1)])
        if "cleanedPodcasts" in db_names:
            self.db.podcasts.create_index([("originalId", 1)])
        return

    def save_article(self, article: dict):
        if not self.article_exists(article["title"], article["author"]):
            return self.db.articles.update_one(article, {"$setOnInsert": article}, upsert=True)

    def save_book(self, book: dict):
        return self.db.books.update_one(book, {"$setOnInsert": book}, upsert=True)

    def save_rss_feed(self, podcast: dict):
        return self.db.podcasts.update_one(podcast, {"$setOnInsert": podcast}, upsert=True)

    def save_cleaned_book(self, book: dict):
        return self.db.cleanedBooks.update_one(book, {"$setOnInsert": book}, upsert=True)

    def save_cleaned_podcast(self, podcast: dict):
        return self.db.cleanedPodcasts.update_one(podcast, {"$setOnInsert": podcast}, upsert=True)

    def replace_article(self, article: dict, new_article: dict):
        return self.db.articles.replace_one(article, new_article)

    def replace_book(self, book: dict, new_book: dict):
        return self.db.books.replace_one(book, new_book)

    def replace_podcast(self, podcast: dict, new_podcast: dict):
        return self.db.podcasts.replace_one(podcast, new_podcast)

    def replace_user(self, user: dict, new_user: dict):
        return self.db.users.replace_one(user, new_user)

    def replace_similarity(self, sim: dict, new_sim: dict):
        return self.db.similarities.replace_one(sim, new_sim)

    def get_parsed_item(self, oid: str, content_type: str):
        if oid != "":
            return self._parse_mongo_objects(self.get_item(oid, content_type))
        elif content_type == "article":
            return self.get_random_articles(1)
        elif content_type == "book":
            return self.get_random_books(1)
        elif content_type == "podcast":
            return self.get_random_podcasts(1)

    def get_item(self, oid: str, content_type: str):
        if content_type == "article":
            return self.db.articles.find_one({"_id": ObjectId(oid)})
        elif content_type == "book":
            return self.db.books.find_one({"_id": ObjectId(oid)})
        elif content_type == "podcast":
            return self.db.podcasts.find_one({"_id": ObjectId(oid)})

    def find_podcast_with_title_and_link(self, title, link):
        podcast = self.db.podcasts.find_one({"title": title, "link": link})
        if podcast is not None:
            return podcast
        else:
            return self.db.podcasts.find_one({"title": title})

    def get_user(self, oid: ObjectId):
        return self.db.users.find_one({"_id": oid})

    def get_user_by_participant_id(self, participant_id: str):
        return self.db.users.find_one({"participantId": participant_id})

    def get_cleaned_podcast_by_original_id(self, oid: ObjectId):
        return self.db.cleanedPodcasts.find_one({"originalId": oid})

    def create_user(self, participant_id: str, personalised: bool):
        data = {"participantId": participant_id, "personalised": personalised, "interests": [], "logs": [],
                "dismissed": []}
        self.db.users.update_one(data, {"$setOnInsert": data}, upsert=True)

    def get_all_articles(self) -> list:
        articles = []
        cursor = self.db.articles.find({})
        for document in cursor:
            articles.append(document)
        return articles

    def get_all_books(self) -> list:
        books = []
        cursor = self.db.books.find({})
        for document in cursor:
            books.append(document)
        return books

    def get_all_similarities(self, content_type: str):
        sims = []
        cursor = self.db.similarities.find({"contentType": content_type})
        for document in cursor:
            sims.append(document)
        return sims

    def get_all_cleaned_books(self) -> list:
        books = []
        cursor = self.db.cleanedBooks.find({})
        for document in cursor:
            books.append(document)
        return books

    def get_all_podcasts(self) -> list:
        podcasts = []
        cursor = self.db.podcasts.find({})
        for document in cursor:
            podcasts.append(document)
        return podcasts

    def get_all_cleaned_podcasts(self) -> list:
        podcasts = []
        cursor = self.db.cleanedPodcasts.find({})
        for document in cursor:
            podcasts.append(document)
        return podcasts

    def get_all_users(self) -> list:
        users = []
        cursor = self.db.users.find({})
        for document in cursor:
            users.append(document)
        return users

    def get_random_articles(self, num: int, blacklist: list = []) -> list:
        if "NEWS" not in blacklist:
            return [article for article in self._parse_mongo_objects(
                self.db.articles.aggregate([{"$sample": {"size": num}}]))]
        else:
            return []

    def get_random_books(self, num: int, blacklist: list = []) -> list:
        if "BOOKS" not in blacklist:
            return [book for book in self._parse_mongo_objects(
                self.db.books.aggregate([{"$sample": {"size": num}}], allowDiskUse=True))]
        else:
            return []

    def get_random_podcasts(self, num: int, blacklist: list = []) -> list:
        if "PODCASTS" not in blacklist:
            return [podcast for podcast in self._parse_mongo_objects(
                self.db.podcasts.aggregate([{"$sample": {"size": num}}]))]
        else:
            return []

    def book_in_cleaned_books(self, oid: ObjectId):
        book = self.db.cleanedBooks.find_one({"originalId": oid})
        if book is not None:
            return True
        else:
            return False

    def podcast_in_cleaned_podcasts(self, oid: ObjectId):
        podcast = self.db.cleanedPodcasts.find_one({"originalId": oid})
        if podcast is not None:
            return True
        else:
            return False

    def similarity_exists(self, oid, content_type, episode_index=None):
        if episode_index is not None:
            sim = self.db.similarities.find_one({"originalId": oid, "contentType": content_type,
                                                 "episodeIndex": episode_index})
        else:
            sim = self.db.similarities.find_one({"originalId": oid, "contentType": content_type})
        if sim is not None:
            return True
        else:
            return False

    def podcast_sim_calculated(self, oid, episode_index):
        podcast_sim = self.db.similarities.find_one({"originalId": oid, "episodeIndex": episode_index})
        if podcast_sim is not None:
            return True
        else:
            return False

    def article_exists(self, title: str, author: str) -> bool:
        article = self.db.articles.find_one({"title": title, "author": author})
        if article is not None:
            return True
        else:
            return False

    def user_exists(self, participant_id: str) -> bool:
        user = self.db.users.find_one({"participantId": participant_id})
        if user is not None:
            return True
        else:
            return False

    def register_interest(self, participant_id: str, content_type: str, oid: str, episode_index: int):
        user = self.db.users.find_one({"participantId": participant_id})
        if type(oid) == str:
            oid = ObjectId(oid)
        if "interests" not in user:
            user["interests"] = []
        if content_type == "podcast":
            interest = {"contentType": content_type, "oid": oid, "added": time.time(),
                        "episodeIndex": episode_index}
        else:
            interest = {"contentType": content_type, "oid": oid, "added": time.time()}
        for old_interest in user["interests"]:
            if interest["oid"] == old_interest["oid"]:
                break
        else:
            user["interests"].append(interest)

        self.db.users.update_one({"participantId": participant_id}, {"$set": user})

    def save_recommendation_scores(self, object_id, content_type, recommendation_scores, most_prominent_topics,
                                   episode_index=None):
        if episode_index is not None:
            data = {"originalId": ObjectId(object_id), "contentType": content_type,
                    "scores": recommendation_scores, "mostProminentTopics": most_prominent_topics,
                    "episodeIndex": episode_index}
        else:
            data = {"originalId": ObjectId(object_id), "contentType": content_type,
                    "scores": recommendation_scores, "mostProminentTopics": most_prominent_topics}
        if not self.similarity_exists(object_id, content_type, episode_index):
            return self.db.similarities.update_one(data, {"$setOnInsert": data}, upsert=True)
        else:
            if episode_index is not None:
                old_sim = self.db.similarities.find_one({"originalId": object_id, "contentType": content_type,
                                                         "episodeIndex": episode_index})
            else:
                old_sim = self.db.similarities.find_one({"originalId": object_id, "contentType": content_type})
            return self.db.similarities.replace_one(old_sim, data)

    def get_interests(self, participant_id: str, cutoff: int, content_type: str) -> list:
        user = self.db.users.find_one({"participantId": participant_id})
        content_interests = []
        if "interests" in user:
            interests = user["interests"]
            for interest in interests:
                if interest["contentType"] == content_type:
                    content_interests.append(interest)
        content_interests.sort(key=lambda x: x['added'], reverse=True)
        return content_interests[:cutoff]

    def get_dismissed(self, participant_id: str) -> list:
        user = self.db.users.find_one({"participantId": participant_id})
        if user is not None:
            if "dismissed" in user:
                return user["dismissed"]
        return []

    def get_personalised(self, participant_id: str) -> bool:
        user = self.db.users.find_one({"participantId": participant_id})
        if user is not None:
            if "personalised" in user:
                return user["personalised"]
        return True

    def get_article_recommendations(self, interests: list, blacklist: list, dismissed: list, difficulty: str) -> list:
        recommendations = []
        now = time.time()
        for interest in interests:
            if not interest["contentType"] == "article":
                continue
            similarities = self.db.similarities.find_one({"originalId": interest['oid'],
                                                          "contentType": "article"})
            if similarities is not None:
                scores = similarities["scores"]
                for score in scores[1:]:
                    article_in_interests = False
                    for inter in interests:
                        if str(score["oid"]) == str(inter["oid"]):
                            article_in_interests = True
                            break
                    article_in_recommendations = False
                    for recommendation in recommendations:
                        if str(score["oid"]) == str(recommendation["recommendation"]["_id"]):
                            article_in_recommendations = True
                            break
                    if "difficulty" in score:
                        desired_difficulty = self._item_fits_desired_difficulty(difficulty, score["difficulty"])
                    else:
                        desired_difficulty = True
                    if article_in_interests is False and article_in_recommendations is False \
                            and desired_difficulty is True:
                        article_to_recommend = self.db.articles.find_one({"_id": score["oid"]})
                        if article_to_recommend is not None and article_to_recommend["source"] not in blacklist \
                                and article_to_recommend["added"] > (now - ARTICLES_HALFLIFE) \
                                and not self._item_in_dismissed(article_to_recommend, dismissed):
                            trigger_article = self.db.articles.find_one({"_id": interest['oid']})
                            recommendations.append({"recommendation": article_to_recommend,
                                                    "trigger": trigger_article})
                            break
        return [self._parse_mongo_objects(recommendation) for recommendation in recommendations]

    def get_book_recommendations(self, interests: list, dismissed: list, difficulty: str) -> list:
        recommendations = []
        for interest in interests:
            if not interest["contentType"] == "book":
                continue
            similarities = self.db.similarities.find_one({"originalId": interest['oid'],
                                                          "contentType": "book"})
            if similarities is not None:
                scores = similarities["scores"]
                for score in scores[1:]:
                    book_in_interests = False
                    for inter in interests:
                        if str(score["oid"]) == str(inter["oid"]):
                            book_in_interests = True
                            break
                    book_in_recommendations = False
                    for recommendation in recommendations:
                        if str(score["oid"]) == str(recommendation["recommendation"]["_id"]):
                            book_in_recommendations = True
                            break
                    if "difficulty" in score:
                        desired_difficulty = self._item_fits_desired_difficulty(difficulty, score["difficulty"])
                    else:
                        desired_difficulty = True
                    if book_in_interests is False and book_in_recommendations is False and desired_difficulty is True:
                        book_to_recommend = self.db.books.find_one({"_id": score["oid"]})
                        if book_to_recommend is not None \
                                and not self._item_in_dismissed(book_to_recommend, dismissed):
                            trigger_book = self.db.books.find_one({"_id": interest['oid']})
                            recommendations.append({"recommendation": book_to_recommend,
                                                    "trigger": trigger_book})
                            break
        return [self._parse_mongo_objects(recommendation) for recommendation in recommendations]

    def get_podcast_recommendations(self, interests: list, dismissed: list, difficulty: str) -> list:
        recommendations = []
        for interest in interests:
            if not interest["contentType"] == "podcast":
                continue
            similarities = self.db.similarities.find_one({"originalId": interest['oid'],
                                                          "contentType": "podcast",
                                                          "episodeIndex": interest["episodeIndex"]})
            if similarities is not None:
                scores = similarities["scores"]
                for score in scores[1:]:
                    podcast_in_interests = False
                    for inter in interests:
                        if str(score["oid"]) == str(inter["oid"]):
                            podcast_in_interests = True
                            break
                    podcast_in_recommendations = False
                    for recommendation in recommendations:
                        if str(score["oid"]) == str(recommendation["recommendation"]["_id"]):
                            podcast_in_recommendations = True
                            break
                    if "difficulty" in score:
                        desired_difficulty = self._item_fits_desired_difficulty(difficulty, score["difficulty"])
                    else:
                        desired_difficulty = True
                    if podcast_in_interests is False and podcast_in_recommendations is False \
                            and desired_difficulty is True:
                        podcast_to_recommend = self.db.podcasts.find_one({"_id": score["oid"]})
                        if podcast_to_recommend is not None \
                                and not self._item_in_dismissed(podcast_to_recommend, dismissed, score["episodeIndex"]):
                            trigger_podcast = self.db.podcasts.find_one({"_id": interest['oid']})
                            recommendations.append({"recommendation": podcast_to_recommend,
                                                    "trigger": trigger_podcast,
                                                    "episodeIndex": score["episodeIndex"]})
                            break
        return [self._parse_mongo_objects(recommendation) for recommendation in recommendations]

    def _item_fits_desired_difficulty(self, user_difficulty, item_difficulty):
        desired_difficulty = False
        if type(user_difficulty) is str:
            try:
                user_difficulty = int(user_difficulty)
            except Exception:
                return desired_difficulty
        if user_difficulty == 0:
            user_difficulty = 1  # dirty hack to prevent A1 level
        difficulty_number = self._difficulty_keymap(item_difficulty)
        lower_diff = difficulty_number - 1
        higher_diff = difficulty_number + 1
        if user_difficulty == difficulty_number or user_difficulty == lower_diff or \
                user_difficulty == higher_diff:
            desired_difficulty = True
        return desired_difficulty

    def _difficulty_keymap(self, difficulty):
        keymap = {"A1/A2": 1, "B1": 2, "B2": 3, "C1": 4, "C2": 5}
        if difficulty in keymap:
            return keymap[difficulty]
        else:
            return 2  # B1 default

    def get_similarity(self, oid: str, content_type: str, episode_index=None):
        if episode_index is None:
            return self.db.similarities.find_one({"originalId": ObjectId(oid), "contentType": content_type})
        else:
            return self.db.similarities.find_one({"originalId": oid, "contentType": content_type,
                                                  "episodeIndex": episode_index})

    def get_similar_items(self, oid: str, content_type: str, cutoff: int, episode_index=None) -> list:
        similar_items = []
        if content_type == "podcast":
            item = self.db.similarities.find_one({"originalId": ObjectId(oid), "contentType": content_type,
                                                  "episodeIndex": episode_index})
        else:
            item = self.db.similarities.find_one({"originalId": ObjectId(oid), "contentType": content_type})
        if item is not None:
            similarity_scores = item["scores"]
            for score in similarity_scores[:cutoff]:
                if content_type == "article":
                    similar_article = self.db.articles.find_one({"_id": score["oid"]})
                    if similar_article is not None:
                        similar_items.append({"item": {
                            "title": similar_article["title"],
                            "body": similar_article["body"]
                        }, "score": score["score"]})
                elif content_type == "book":
                    similar_book = self.db.books.find_one({"_id": score["oid"]})
                    if similar_book is not None and similar_book["text"] is not None:
                        similar_items.append({"item": {
                            "title": similar_book["title"],
                            "body": " ".join(similar_book["text"][:3])
                        }, "score": score["score"]})
                elif content_type == "podcast":
                    similar_podcast = self.db.podcasts.find_one({"_id": score["oid"]})
                    if similar_podcast is not None:
                        similar_episode = similar_podcast["entries"][score["episodeIndex"]]
                        similar_items.append({"item": {
                            "title": similar_episode["title"],
                            "body": similar_episode["description"]
                        }, "score": score["score"]})
        similar_items = self._parse_mongo_objects(similar_items)
        return similar_items

    def get_item_keywords(self, oid: str, content_type: str, episode_index=0) -> list:
        keywords = []
        if content_type == "podcast":
            item = self.db.similarities.find_one({"originalId": ObjectId(oid), "contentType": content_type,
                                                  "episodeIndex": episode_index})
        else:
            item = self.db.similarities.find_one({"originalId": ObjectId(oid), "contentType": content_type})
        if item is not None:
            raw_keywords = item["mostProminentTopics"]
            pattern = re.compile(r"\"[A-Za-zÀ-ſ]+\"")
            for raw_keyword_match in re.findall(pattern, raw_keywords):
                keyword = raw_keyword_match.replace('"', '')
                keywords.append(keyword)
        return keywords

    def delete_article(self, oid):
        self.db.articles.delete_one({"_id": oid})

    def delete_podcast(self, oid):
        self.db.podcasts.delete_one({"_id": oid})

    def delete_interest(self, oid, content_type, user_oid):
        if type(oid) is ObjectId:
            oid = str(oid)
        user = self.get_user(user_oid)
        new_user = copy.deepcopy(user)
        if "interests" in user:
            new_interests = []
            for interest in user["interests"]:
                if interest["oid"] != oid and interest["contentType"] != content_type:
                    new_interests.append(interest)
            new_user["interests"] = new_interests
        self.replace_user(user, new_user)

    def delete_item_from_similar_items(self, oid, content_type, episode_index=None):
        if episode_index:
            self.db.similarities.delete_many({"originalId": oid, "contentType": content_type, "episodeIndex": episode_index})
        else:
            self.db.similarities.delete_many({"originalId": oid, "contentType": content_type})

    def delete_podcasts_from_similar_items(self):
        self.db.similarities.delete_many({"contentType": "podcast"})

    def delete_content_type_from_similar_items(self, content_type: str):
        self.db.similarities.delete_many({"contentType": content_type})

    def clear_collection_cache(self, collection_name: str):
        return self.db.command({"planCacheClear": collection_name})

    def add_logs(self, participant_id: str, logs: list):
        user = self.db.users.find_one({"participantId": participant_id})
        new_user = copy.deepcopy(user)
        if type(logs) is str:
            logs = json.loads(logs)
        for log in logs:
            log.pop("synced", None)
            if "oid" in log and type(log["oid"]) is str and "oid" != "" and ObjectId.is_valid(log["oid"]):
                log["oid"] = ObjectId(log["oid"])
            elif "oid" in log:  # oid is malformed for some reason
                log.pop("oid")
        if "logs" not in new_user:
            new_user["logs"] = []
        new_user["logs"].extend(logs)
        self.replace_user(user, new_user)
        return {"status": 200}

    def add_dismissed_item(self, participant_id: str, oid: str, content_type: str, episode_index: str):
        user = self.get_user_by_participant_id(participant_id)
        if user is not None:
            new_user = copy.deepcopy(user)
            if "dismissed" not in new_user:
                new_user["dismissed"] = []
            if type(oid) is str:
                oid = ObjectId(oid)
            if content_type == "podcast":
                new_user["dismissed"].append({"oid": oid, "contentType": content_type, "episodeIndex": episode_index})
            else:
                new_user["dismissed"].append({"oid": oid, "contentType": content_type})
            self.replace_user(user, new_user)

    def _atoi(self, text):
        return int(text) if text.isdigit() else text

    def _sort_datetime(self, element):
        '''
        alist.sort(key=natural_keys) sorts in human order
        http://nedbatchelder.com/blog/200712/human_sorting.html
        (See Toothy's implementation in the comments)
        '''
        return [self._atoi(c) for c in re.split(r'(\d+)', element["added"])]

    def _parse_mongo_objects(self, object: dict) -> dict:
        return json.loads(json_util.dumps(object))

    def _item_in_dismissed(self, item: dict, dismissed: list, episode_index=None) -> bool:
        if episode_index is not None:
            for d in dismissed:
                if item["_id"] == d["oid"] and episode_index == d["episodeIndex"]:
                    return True
        else:
            for d in dismissed:
                if item["_id"] == d["oid"]:
                    return True
        return False

    def update_podcast(self, podcast: dict):
        return self.db.podcasts.update_one({'_id': podcast['_id']}, {'$set': podcast}, upsert=False)

    def update_cleaned_podcast(self, podcast: dict):
        return self.db.cleanedPodcasts.update_one({'_id': podcast['_id']}, {'$set': podcast}, upsert=False)

    def replace_cleaned_podcast(self, old_podcast: dict, new_podcast: dict):
        return self.db.cleanedPodcasts.replace_one(old_podcast, new_podcast)

    def add_difficulty_to_books(self):
        from string_processing import StringProcessing
        sp = StringProcessing()
        books = self.get_all_books()
        for i, book in enumerate(books):
            if "difficulty" not in book:
                new_book = copy.deepcopy(book)
                readability_index = sp.calculate_readability_index(" ".join(book["text"]))
                difficulty = sp.get_difficulty_from_readability_index(readability_index)
                new_book["difficulty"] = difficulty
                self.replace_book(book, new_book)
                print(f"\rupdated book number {i}", end="")

    def add_difficulty_to_podcasts(self):
        from string_processing import StringProcessing
        sp = StringProcessing()
        podcasts = self.get_all_podcasts()
        for i, podcast in enumerate(podcasts):
            new_podcast = copy.deepcopy(podcast)
            new_entries = []
            did_something = False
            for entry in podcast["entries"]:
                new_entry = copy.deepcopy(entry)
                if "difficulty" not in entry:
                    if "description" in entry:
                        readability_index = sp.calculate_readability_index(entry["description"])
                        difficulty = sp.get_difficulty_from_readability_index(readability_index)
                    elif "summary" in entry:
                        readability_index = sp.calculate_readability_index(entry["summary"])
                        difficulty = sp.get_difficulty_from_readability_index(readability_index)
                    else:
                        difficulty = ""
                    new_entry["difficulty"] = difficulty
                    did_something = True
                new_entries.append(new_entry)
            if did_something:
                new_podcast["entries"] = new_entries
                try:
                    self.replace_podcast(podcast, new_podcast)
                except DocumentTooLarge:
                    print(f"\ndocument too large for{podcast['_id']}")
                print(f"\rreplaced podcast {i}", end="")

    def add_difficulty_to_articles(self):
        from string_processing import StringProcessing
        sp = StringProcessing()
        articles = self.get_all_articles()
        for article in articles:
            if "difficulty"  not in article:
                new_article = copy.deepcopy(article)
                readability_index = sp.calculate_readability_index(article["body"])
                difficulty = sp.get_difficulty_from_readability_index(readability_index)
                new_article["difficulty"] = difficulty
                self.replace_article(article, new_article)

    def delete_zombie_similarities(self):
        with client.start_session() as session:
            similarity_cursor = self.db.similarities.find({"contentType": "article"}, session=session, no_cursor_timeout=True)
            item_index = 1
            items_to_delete = []
            while True:
                try:
                    sims = similarity_cursor.next()
                    item = self.get_item(sims["originalId"], sims["contentType"])
                    if item is None:
                        items_to_delete.append(sims)
                    item_index += 1
                except StopIteration:
                    break
            similarity_cursor.close()
            for item in items_to_delete:
                if item["contentType"] == "podcast":
                    self.delete_item_from_similar_items(item["originalId"], sims["contentType"], sims["episodeIndex"])
                else:
                    self.delete_item_from_similar_items(item["originalId"], sims["contentType"])
            print(f"deleted {len(items_to_delete)} from {item_index} sims")

    def assess_difficulty_distribution(self):
        print("articles:")
        articles = self.get_all_articles()
        article_difficulty_dist = {}
        for article in articles:
            if "difficulty" in article:
                if article["difficulty"] in article_difficulty_dist:
                    article_difficulty_dist[article["difficulty"]] += 1
                else:
                    article_difficulty_dist[article["difficulty"]] = 1
            else:
                if "none" in article_difficulty_dist:
                    article_difficulty_dist["none"] += 1
                else:
                    article_difficulty_dist["none"] = 1
        for key in article_difficulty_dist.keys():
            print(f"{key}: {article_difficulty_dist[key]} of {len(articles)} "
                  f"( {100*article_difficulty_dist[key]/len(articles)} )")
        articles = None
        print("books:")
        books = self.get_all_books()
        book_difficulty_dist = {}
        for book in books:
            if "difficulty" in book:
                if book["difficulty"] in book_difficulty_dist:
                    book_difficulty_dist[book["difficulty"]] += 1
                else:
                    book_difficulty_dist[book["difficulty"]] = 1
            else:
                if "none" in book_difficulty_dist:
                    book_difficulty_dist["none"] += 1
                else:
                    book_difficulty_dist["none"] = 1
        for key in book_difficulty_dist.keys():
            print(f"{key}: {book_difficulty_dist[key]} of {len(books)} ( {100*book_difficulty_dist[key]/len(books)} )")
        books = None
        print("podcasts:")
        podcasts = self.get_all_podcasts()
        podcast_difficulty_dist = {}
        episode_count = 0
        for podcast in podcasts:
            for episode in podcast["entries"]:
                episode_count += 1
                if "difficulty" in episode:
                    if episode["difficulty"] in podcast_difficulty_dist:
                        podcast_difficulty_dist[episode["difficulty"]] += 1
                    else:
                        podcast_difficulty_dist[episode["difficulty"]] = 1
                else:
                    if "none" in podcast_difficulty_dist:
                        podcast_difficulty_dist["none"] += 1
                    else:
                        podcast_difficulty_dist["none"] = 1
        for key in podcast_difficulty_dist.keys():
            print(f"{key}: {podcast_difficulty_dist[key]} of {episode_count} "
                  f"( {100 * podcast_difficulty_dist[key] / episode_count} )")
        podcasts = None
        return

    def add_difficulty_to_sims(self):
        with client.start_session() as session:
            similarity_cursor = self.db.similarities.find({"contentType": "article"}, session=session, no_cursor_timeout=True)
            schedule.every(5).minutes.do(self._session_refresh, session)
            loop = asyncio.get_event_loop()
            has_next = True
            sim_index = 1
            retrieved_difficulties = {}
            while has_next:
                try:
                    loop.run_until_complete(schedule.run_pending())
                    sims = similarity_cursor.next()
                    did_something = False
                    if "contentType" in sims:
                        new_scores = []
                        for score in sims["scores"]:
                            if "episodeIndex" in score:
                                suffix = "_" + str(score["episodeIndex"])
                            else:
                                suffix = ""
                            diff_key = f"{sims['contentType']}_{score['oid']}{suffix}"
                            new_score = copy.deepcopy(score)
                            try:
                                if "difficulty" not in score:
                                    if diff_key in retrieved_difficulties:
                                        difficulty = retrieved_difficulties[diff_key]
                                        new_score["difficulty"] = difficulty
                                        did_something = True
                                    else:
                                        item = self.get_item(score["oid"], sims["contentType"])
                                        if sims["contentType"] == "podcast":
                                            item = item["entries"][score["episodeIndex"]]
                                        if "difficulty" in item:
                                            difficulty = item["difficulty"]
                                            new_score["difficulty"] = difficulty
                                            retrieved_difficulties[diff_key] = difficulty
                                            did_something = True
                                else:
                                    if diff_key not in retrieved_difficulties:
                                        retrieved_difficulties[diff_key] = score["difficulty"]
                            except Exception as e:
                                print(e)
                            new_scores.append(new_score)
                    if did_something:
                        new_sims = copy.deepcopy(sims)
                        new_sims["scores"] = new_scores
                        self.replace_similarity(sims, new_sims)
                    print(f"\r processed for index: {sim_index}, type {sims['contentType']}", end="")
                    sim_index += 1
                except StopIteration:
                    has_next = False
            similarity_cursor.close()

    async def _session_refresh(self, session):
        self.db.command(
            'refreshSessions', [session.session_id], session=session)

    def fix_links_in_podcasts(self):
        podcasts = self.get_all_podcasts()
        podcasts_to_delete = []
        for podcast in podcasts:
            found_dirty = False
            for entry in podcast["entries"]:
                if not entry["link"]:
                    found_dirty = True
                    break
            if found_dirty:
                podcasts_to_delete.append(podcast["_id"])
        for p_id in podcasts_to_delete:
            self.delete_podcast(p_id)
            self.delete_item_from_similar_items(p_id, "podcast")
        print(f"deleted {len(podcasts_to_delete)} podcasts and their sims")
        with client.start_session() as session:
            similarity_cursor = self.db.similarities.find({"contentType": "podcast"}, session=session, batch_size=100000,
                                                          no_cursor_timeout=True)
            schedule.every(5).minutes.do(self._session_refresh, session)
            loop = asyncio.get_event_loop()
            sim_index = 0
            replace_index = 0
            while True:
                try:
                    loop.run_until_complete(schedule.run_pending())
                    sims = similarity_cursor.next()
                    new_scores = []
                    new_sims = copy.deepcopy(sims)
                    for score in sims["scores"]:
                        if score["oid"] not in podcasts_to_delete:
                            new_scores.append(score)
                    new_sims["scores"] = new_scores
                    if len(sims["scores"]) > len(new_sims["scores"]):
                        replace_index += 1
                        self.replace_similarity(sims, new_sims)
                    print(f"\rhandled podcast sim {sim_index}", end="")
                    sim_index += 1
                except StopIteration:
                    break
            print(f"\nreplaced {replace_index} sims")
        for user in self.get_all_users():
            for p_id in podcasts_to_delete:
                self.delete_interest(p_id, "podcast", user["_id"])

    def save_article_to_archive(self, article):
        return self.db.articleArchive.update_one(article, {"$setOnInsert": article}, upsert=True)

    def get_article_from_archive(self, oid: str) -> dict:
        return self.db.articleArchive.find_one({"_id": ObjectId(oid)})

if __name__ == "__main__":
    database = Database()

